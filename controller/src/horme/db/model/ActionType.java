package horme.db.model;

public enum ActionType {
    EMAIL,
    SMS,
    PHONE;
}
