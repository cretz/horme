package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable
public class Agent {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent
    @Column(name = "IpAddress", length = 39)
    private String ipAddress;
    
    @Persistent
    @Column(name = "Heartbeat", allowsNull = "false")
    private long heartbeat;
    
    @Persistent
    @Column(name = "PrimaryControllerId")
    private Controller primaryController;
    
    @Persistent
    @Column(name = "PendingUpdate", defaultValue = "false", allowsNull = "false")
    private boolean pendingUpdate;
    
    @Persistent
    @Column(name = "ControllerGroupId")
    private ControllerGroup controllerGroup;

    @Persistent(mappedBy = "agent")
    private Set<AgentMetric> agentMetrics;
    
    @Persistent(table = "Agent_AgentGroup")
    @Join(column = "AgentId")
    @Element(column = "AgentGroupId")
    private Set<AgentGroup> agentGroups;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getHeartbeat() {
        return heartbeat;
    }

    public void setHeartbeat(long heartbeat) {
        this.heartbeat = heartbeat;
    }

    public Controller getPrimaryController() {
        return primaryController;
    }

    public void setPrimaryController(Controller primaryController) {
        this.primaryController = primaryController;
    }
    
    public boolean isPendingUpdate() {
        return pendingUpdate;
    }
    
    public void setPendingUpdate(boolean pendingUpdate) {
        this.pendingUpdate = pendingUpdate;
    }

    public ControllerGroup getControllerGroup() {
        return controllerGroup;
    }
    
    public void setControllerGroup(ControllerGroup controllerGroup) {
        this.controllerGroup = controllerGroup;
    }

    public Set<AgentMetric> getAgentMetrics() {
        return agentMetrics;
    }

    public void setAgentMetrics(Set<AgentMetric> agentMetrics) {
        this.agentMetrics = agentMetrics;
    }
    
    public Set<AgentGroup> getAgentGroups() {
        return agentGroups;
    }
    
    public void setAgentGroups(Set<AgentGroup> agentGroups) {
        this.agentGroups = agentGroups;
    }
}
