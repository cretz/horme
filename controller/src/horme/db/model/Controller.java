package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Unique;

@PersistenceCapable(table = "Controller")
public class Controller {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Unique
    @Column(name = "Name", length = 255)
    private String name;

    @Persistent
    @Column(name = "Host", length = 255)
    private String host;

    @Persistent
    @Column(name = "Port")
    private int port;
    
    @Persistent
    @Column(name = "Mac", length = 100)
    private String mac;
    
    @Persistent(mappedBy = "primaryController")
    private Set<Agent> primaryAgents;
    
    @Persistent
    @Column(name = "ControllerGroupId")
    private ControllerGroup controllerGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public String getMac() {
        return mac;
    }
    
    public void setMac(String mac) {
        this.mac = mac;
    }
    
    public Set<Agent> getPrimaryAgents() {
        return primaryAgents;
    }
    
    public void setPrimaryAgents(Set<Agent> primaryAgents) {
        this.primaryAgents = primaryAgents;
    }
    
    public ControllerGroup getControllerGroup() {
        return controllerGroup;
    }
    
    public void setControllerGroup(ControllerGroup controllerGroup) {
        this.controllerGroup = controllerGroup;
    }
}
