package horme.db.model;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public enum MetricDataType {
    BOOL {
        @Override
        public Boolean convertValue(String value) {
            return Boolean.valueOf(value);
        }
    },
    BYTE {
        @Override
        public Byte convertValue(String value) {
            return Byte.valueOf(value);
        }
    },
    DOUBLE {
        @Override
        public BigDecimal convertValue(String value) {
            return new BigDecimal(value);
        }
    },
    I16 {
        @Override
        public Short convertValue(String value) {
            return Short.valueOf(value);
        }
    },
    I32 {
        @Override
        public Integer convertValue(String value) {
            return Integer.valueOf(value);
        }
    },
    I64 {
        @Override
        public Object convertValue(String value) {
            return Long.valueOf(value);
        }
    },
    LIST {
        @Override
        public List<String> convertValue(String value) {
            return Arrays.asList(value.split(","));
        }
    },
    MAP {
        @Override
        public Map<String, String> convertValue(String value) {
            //TODO
            throw new UnsupportedOperationException();
        }
    },
    STRING {
        @Override
        public String convertValue(String value) {
            return value;
        }
    },
    UI64 {
        @Override
        public String convertValue(String value) {
            //TODO
            throw new UnsupportedOperationException();
        }
    };
    
    public abstract Object convertValue(String value);
    
    public boolean isValid(String value) {
        try {
            Object ret = convertValue(value);
            return ret != null;
        } catch (Exception e) {
            return false;
        }
    }
}
