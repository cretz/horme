package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(table = "ControllerGroup")
public class ControllerGroup {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent(mappedBy = "controllerGroup")
    private Set<Controller> controllers;
    
    @Persistent(mappedBy = "controllerGroup")
    private Set<Agent> agents;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public Set<Controller> getControllers() {
        return controllers;
    }

    public void setControllers(Set<Controller> controllers) {
        this.controllers = controllers;
    }
    
    public Set<Agent> getAgents() {
        return agents;
    }
    
    public void setAgents(Set<Agent> agents) {
        this.agents = agents;
    }
}
