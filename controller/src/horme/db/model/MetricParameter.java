package horme.db.model;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;

@PersistenceCapable(table = "MetricParameter")
public class MetricParameter {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;

    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent
    @Column(name = "FriendlyName", length = 255)
    private String friendlyName;
    
    @Persistent
    @Column(name = "DataType", length = 10, allowsNull = "false")
    private MetricDataType dataType;
    
    @Persistent
    @Column(name = "DefaultValue", length = 255)
    private String defaultValue;
    
    @Persistent
    @Column(name = "Required", allowsNull = "false", defaultValue = "false")
    private boolean required;
    
    @Persistent
    @Column(name = "MetricId")
    private Metric metric;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public MetricDataType getDataType() {
        return dataType;
    }

    public void setDataType(MetricDataType dataType) {
        this.dataType = dataType;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    public boolean isRequired() {
        return required;
    }
    
    public void setRequired(boolean required) {
        this.required = required;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }
}
