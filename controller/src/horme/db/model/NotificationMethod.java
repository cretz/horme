package horme.db.model;

public enum NotificationMethod {
    EMAIL,
    PHONE,
    SMS,
    HTTP_POST
}
