package horme.db.model;

import java.util.Map;
import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.Key;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Value;

@PersistenceCapable
public class AgentMetric {
    
    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent
    @Column(name = "MetricId")
    private Metric metric;
    
    @Persistent
    @Column(name = "ExecutionFrequency")
    private long executionFrequency;
    
    @Persistent
    @Column(name = "HaltOnFailure")
    private boolean haltOnFailure;
    
    @Persistent
    @Column(name = "AgentId")
    private Agent agent;
    
    @Persistent(table = "AgentMetric_Param")
    @Join(column = "AgentMetricId")
    @Key(column = "ParamName")
    @Value(column = "ParamValue")
    private Map<String, String> params;
    
    @Persistent(table = "AgentMetric_AgentMetricGroup")
    @Join(column = "AgentMetricId")
    @Element(column = "AgentMetricGroupId")
    private Set<AgentMetricGroup> agentMetricGroups;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public long getExecutionFrequency() {
        return executionFrequency;
    }

    public void setExecutionFrequency(long executionFrequency) {
        this.executionFrequency = executionFrequency;
    }

    public boolean isHaltOnFailure() {
        return haltOnFailure;
    }

    public void setHaltOnFailure(boolean haltOnFailure) {
        this.haltOnFailure = haltOnFailure;
    }
    
    public Agent getAgent() {
        return agent;
    }
    
    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
    
    public Set<AgentMetricGroup> getAgentMetricGroups() {
        return agentMetricGroups;
    }
    
    public void setAgentMetricGroups(Set<AgentMetricGroup> agentMetricGroups) {
        this.agentMetricGroups = agentMetricGroups;
    }
}
