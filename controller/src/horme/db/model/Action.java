package horme.db.model;

import java.util.Map;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.Key;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Value;

@PersistenceCapable(table = "Action")
public class Action {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent
    @Column(name = "Type", length = 50, allowsNull = "false")
    private ActionType type;
    
    @Persistent(table = "Action_Param")
    @Join(column = "ActionId")
    @Key(column = "ParamName")
    @Value(column = "ParamValue")
    private Map<String, String> params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ActionType getType() {
        return type;
    }

    public void setType(ActionType type) {
        this.type = type;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
