package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Unique;

@PersistenceCapable(table = "AgentMetricGroup")
public class AgentMetricGroup {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Unique
    @Column(name = "Name", length = 255)
    private String name;

    @Persistent(mappedBy = "agentMetricGroups")    
    private Set<AgentMetric> agentMetrics;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AgentMetric> getAgentMetrics() {
        return agentMetrics;
    }

    public void setAgentMetrics(Set<AgentMetric> agentMetrics) {
        this.agentMetrics = agentMetrics;
    }
}
