package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Unique;

@PersistenceCapable(table = "MetricConditional")
public class MetricConditional {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Unique
    @Column(name = "Name", length = 255)
    private String name;

    @Persistent
    @Column(name = "MetricId", allowsNull = "false")
    private Metric metric;

    @Persistent
    @Column(name = "Predicate", allowsNull = "false", length = 1000)
    private String predicate;
    
    @Persistent(table = "MetricConditional_MetricConditionalGroup")
    @Join(column = "MetricConditionalId")
    @Element(column = "MetricConditionalGroupId")
    private Set<MetricConditionalGroup> metricConditionalGroups;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    public String getPredicate() {
        return predicate;
    }

    public void setPredicate(String predicate) {
        this.predicate = predicate;
    }
}
