package horme.db.model;

import java.util.Set;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Element;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Unique;

@PersistenceCapable(table = "Metric")
public class Metric {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Unique
    @Column(name = "Name", length = 255, allowsNull = "false")
    private String name;

    @Persistent
    @Column(name = "FriendlyName", length = 255, allowsNull = "false")
    private String friendlyName;
    
    @Persistent
    @Column(name = "Summary")
    private String summary;

    @Persistent
    @Column(name = "DataType", length = 10, allowsNull = "false")
    private MetricDataType dataType;
    
    @Persistent
    @Column(name = "MetricPackageId", allowsNull = "false")
    private MetricPackage metricPackage;

    @Persistent(table = "Metric_Platform")
    @Join(column = "MetricId")
    @Element(column = "Platform")
    private Set<Platform> platforms;
    
    @Persistent(mappedBy = "metric")
    private Set<MetricParameter> parameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public MetricDataType getDataType() {
        return dataType;
    }
    
    public void setDataType(MetricDataType dataType) {
        this.dataType = dataType;
    }
    
    public MetricPackage getMetricPackage() {
        return metricPackage;
    }
    
    public void setMetricPackage(MetricPackage metricPackage) {
        this.metricPackage = metricPackage;
    }

    public Set<Platform> getPlatforms() {
        return platforms;
    }
    
    public void setPlatforms(Set<Platform> platforms) {
        this.platforms = platforms;
    }
    
    public Set<MetricParameter> getParameters() {
        return parameters;
    }
    
    public void setParameters(Set<MetricParameter> parameters) {
        this.parameters = parameters;
    }
}
