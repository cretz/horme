package horme.db.model;

import java.util.Map;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.Join;
import javax.jdo.annotations.Key;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.Value;

@PersistenceCapable(table = "Notification")
public class Notification {

    @Persistent(customValueStrategy = "uuid", primaryKey = "true")
    @Column(name = "Id", length = 36)
    private String id;
    
    @Persistent
    @Column(name = "Name", length = 255)
    private String name;
    
    @Persistent
    @Column(name = "Method", length = 20, allowsNull = "false")
    private NotificationMethod method;
    
    @Persistent(table = "Notification_Param")
    @Join(column = "NotificationId")
    @Key(column = "ParamName")
    @Value(column = "ParamValue")
    private Map<String, String> params;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public NotificationMethod getMethod() {
        return method;
    }

    public void setMethod(NotificationMethod method) {
        this.method = method;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }
}
