package horme.db.model;

public enum Platform {
    WINDOWS,
    WINDOWS_SERVER,
    LINUX,
    OSX,
    BSD,
    POSIX    
}
