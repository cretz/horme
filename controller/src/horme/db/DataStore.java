package horme.db;

import horme.db.model.Controller;

import java.util.Collection;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;

public class DataStore {

    private final PersistenceManager pm;
    
    public DataStore(PersistenceManager pm) {
        this.pm = pm;
    }
    
    public void close() {
        pm.close();
    }
    
    public Transaction currentTransaction() {
        return pm.currentTransaction();
    }
    
    public <T> T getById(Class<T> clazz, String id) {
        return pm.getObjectById(clazz, id);
    }
    
    public <T> T persist(T object) {
        return pm.makePersistent(object);
    }
    
    public void delete(Object object) {
        pm.deletePersistent(object);
    }
    
    public <T> T detach(T object) {
        return pm.detachCopy(object);
    }
    
    @SuppressWarnings("unchecked")
    public <T> Collection<T> getAll(Class<T> clazz) {
        return (Collection<T>) pm.newQuery(clazz).execute();
    }
    
    public String getControllerIdByMac(String mac) {
        Query query = pm.newQuery(Controller.class);
        query.declareParameters("String myMac");
        query.setFilter("mac == myMac");
        query.setUnique(true);
        Controller controller = (Controller) query.execute(mac);
        return controller == null ? null : controller.getId();
    }
    
    @SuppressWarnings("unchecked")
    public <T> T getByIdOrName(Class<T> clazz, String id) {
        Query query = pm.newQuery(clazz);
        query.declareParameters("String idParam");
        query.setFilter("name == idParam || id == idParam");
        query.setRange(0, 2);
        Collection<T> results = (Collection<T>) query.execute(id);
        if (results.size() == 1) {
            return results.iterator().next();
        } else {
            //we couldn't find a unique one
            return null;
        }
    }
}
