package horme;

import horme.conf.Configuration;
import horme.db.DataStore;
import horme.db.model.Controller;
import horme.db.model.ControllerGroup;
import horme.metric.MetricDbInitializer;
import horme.rest.RestServer;
import horme.thrift.ThriftServer;
import horme.util.NetUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Transaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.io.Closeables;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);
    
    public static int DEFAULT_REST_SERVICE_PORT = 8700;
    public static int DEFAULT_METRIC_LISTENER_PORT = 8710;
    
    public static void main(String[] args) throws Exception {
        Main main = null;
        try {
            //grab create param
            List<String> argList = new ArrayList<String>(Arrays.asList(args));
            boolean create = argList.remove("-create");
            //grab filename
            String filename = null;
            if (argList.size() > 1) {
                throw new HormeException("Too many parameters");
            } else if (!argList.isEmpty()) {
                filename = argList.remove(0);
            }
            //load
            main = new Main(loadConf(filename), create);
            //only start if not creating
            if (!create) {
                main.start();
            }
        } catch (Exception e) {
            System.err.println("Unable to start application: " + e.getMessage());
            logger.error("Unable to start application", e);
        } finally {
            if (main != null) {
                main.stop();
            }
        }
    }

    private static Configuration loadConf(String confFilename) throws FileNotFoundException {
        FileReader confReader = null;
        try {
            //load the conf
            File confFile;
            if (confFilename != null) {
                confFile = new File(confFilename);
            } else {
                confFile = new File("controller-conf.json");
            }
            //exists?
            if (!confFile.exists()) {
                throw new HormeException("Unable to find configuration file: " + confFile);
            }
            //load
            confReader = new FileReader(confFile);
            return GlobalState.newGson().fromJson(confReader, Configuration.class);
        } finally {
            Closeables.closeQuietly(confReader);
        }
    }
    
    private final GlobalState global;
    private final RestServer restServer;
    private final ThriftServer thriftServer;
    
    public Main(Configuration conf, boolean create) throws Exception {
        global = new GlobalState();
        //load conf
        global.setConf(conf);
        //create pmf
        global.setPersistenceManagerFactory(loadPmf(global.getConf()));
        //init
        init(global, create);
        //start thrift server if present
        if (global.getConf().getMetricListener() != null && 
                global.getConf().getMetricListener().getPort() != null) {
            thriftServer = new ThriftServer(global);
        } else {
            thriftServer = null;
        }
        //start rest service if present
        if (global.getConf().getRestService() != null &&
                global.getConf().getRestService().getPort() != null) {
            restServer = new RestServer(global);
        } else {
            restServer = null;
        }
    }
    
    private PersistenceManagerFactory loadPmf(Configuration conf) {
        //get props
        Properties props = new Properties();
        //TODO: put DBCP on classpath please
        //props.setProperty("datanucleus.connectionPoolingType", "DBCP");
        //load from Db
        if (conf.getDatabase() == null || conf.getDatabase().getVendor() == null) {
            throw new HormeException("Database with vendor must be set in config");
        }
        conf.getDatabase().getVendor().applyProperties(conf.getDatabase(), props);
        //create PMF
        logger.info("Connecting to database");
        return JDOHelper.getPersistenceManagerFactory(props);
    }
    
    public void start() throws Exception {
        if (thriftServer != null) {
            logger.info("Starting metric listener");
            thriftServer.start();
        }
        if (restServer != null) {
            logger.info("Starting rest service");
            restServer.start();
        }
    }
    
    public void stop() {
        if (thriftServer != null) {
            try {
                logger.info("Stopping metric listener");
                thriftServer.stop();
            } catch (Exception e) {
                logger.warn("Unable to stop thrift server", e);
            }
        }
        if (restServer != null) {
            try {
                logger.info("Stopping rest service");
                restServer.stop();
            } catch (Exception e) {
                logger.warn("Unable to stop REST server", e);
            }
        }
    }
    
    public GlobalState getGlobal() {
        return global;
    }
    
    private void init(GlobalState global, boolean create) throws Exception {
        DataStore dataStore = new DataStore(global.
                getPersistenceManagerFactory().getPersistenceManager());
        Transaction tx = dataStore.currentTransaction();
        try {
            tx.begin();
            //init?
            String mac = NetUtils.getMacAddress();
            if (create) {
                //create my controller reference
                logger.info("Initializing controller");
                Controller me = new Controller();
                me.setControllerGroup(new ControllerGroup());
                me.setMac(mac);
                dataStore.persist(me);
                //also init the metrics
                new MetricDbInitializer().persistAllPackages(global.getGson(), dataStore);
            }
            tx.commit();
            tx.begin();
            global.setMyControllerId(dataStore.getControllerIdByMac(mac));
            if (global.getMyControllerId() == null) {
                throw new HormeException("Controller not found for mac: " + Arrays.asList(mac));
            }
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            dataStore.close();
        }
    }
}
