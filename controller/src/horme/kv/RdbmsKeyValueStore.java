package horme.kv;

import horme.GlobalState;
import horme.HormeException;
import horme.kv.model.Aggregate;
import horme.kv.model.AggregateDimension;
import horme.kv.model.AggregateDuration;
import horme.kv.model.MetricData;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.datastore.JDOConnection;

/**
 * RDBMS implementation of key-value store. This is probably
 * the slowest option and another key-value store should be
 * used if possible.
 * 
 * @author Chad Retz
 */
public class RdbmsKeyValueStore implements KeyValueStore {
    
    private static final String INCREMENT_INSERT_STMT =
            "INSERT INTO MetricData " +
            "(DataCount, DataTotal, DimensionOneId, DimensionOneType, DimensionTwoId, " +
            "    DimensionTwoType, DataDuration, RangeStart, RangeEnd) " +
            "VALUES (1, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String INCREMENT_UPDATE_STMT =
            "UPDATE MetricData SET " +
            "    DataCount = DataCount + 1," +
            "    DataTotal = DataTotal + ?" +
            "WHERE DimensionOneId = ? " +
            "  AND DimensionOneType = ? " +
            "  AND DimensionTwoId = ? " +
            "  AND DimensionTwoType = ? " +
            "  AND DataDuration = ? " +
            "  AND RangeStart = ? " +
            "  AND RangeEnd = ?";
    
    //TODO: configurable?
    private static final int BATCH_SIZE = 50;
    
    private final GlobalState global;
    
    public RdbmsKeyValueStore(GlobalState global) {
        this.global = global;
    }
    
    @Override
    public void addMetricData(List<MetricData> metricData) {
        // TODO Auto-generated method stub
        
    }
    
    @Override
    public List<MetricData> getMetricData(List<String> metricIds, Long start,
            Long end, int resultLimit) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public List<Aggregate> getAggregates(String dimensionOneId,
            AggregateDimension dimensionOneType, String dimensionTwoId,
            AggregateDimension dimensionTwoType, AggregateDuration duration,
            Long start, Long end, int resultLimit) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void incrementAggregates(Collection<Aggregate> increments) {
        PersistenceManager pm = global.getPersistenceManagerFactory().getPersistenceManager();
        JDOConnection jdoConn = pm.getDataStoreConnection();
        Connection conn = (Connection) jdoConn.getNativeConnection();
        PreparedStatement insertStmt = null;
        PreparedStatement updateStmt = null;
        try {
            //go ahead and prepare
            insertStmt = conn.prepareStatement(INCREMENT_INSERT_STMT);
            updateStmt = conn.prepareStatement(INCREMENT_UPDATE_STMT);
            //do batches
            int pendingInsertBatch = 0;
            List<Object[]> pendingUpdateParams = new ArrayList<Object[]>(BATCH_SIZE);
            //loop
            for (Aggregate aggregate : increments) {
                //build params
                Object[] params = new Object[] {
                        aggregate.getTotal(),
                        aggregate.getDimensionOneId(),
                        aggregate.getDimensionOneType().name(),
                        aggregate.getDimensionTwoId(),
                        aggregate.getDimensionTwoType().name(),
                        aggregate.getDuration().name(),
                        aggregate.getRangeStart(),
                        aggregate.getRangeEnd()
                };
                //insert for MS
                if (aggregate.getDuration() == AggregateDuration.MILLISECOND) {
                    addBatch(insertStmt, params);
                    pendingInsertBatch++;
                } else {
                    //otherwise, attempt update
                    addBatch(updateStmt, params);
                    pendingUpdateParams.add(params);
                    //ready for update batch?
                    if (pendingUpdateParams.size() >= BATCH_SIZE) {
                        int[] updates = updateStmt.executeBatch();
                        for (int i = 0; i < updates.length; i++) {
                            if (updates[i] == 0) {
                                //nope, insert instead
                                addBatch(insertStmt, pendingUpdateParams.get(i));
                                pendingInsertBatch++;
                            }
                        }
                        pendingUpdateParams.clear();
                    }
                }
                //ready for insert batch?
                if (pendingInsertBatch >= BATCH_SIZE) {
                    insertStmt.executeBatch();
                    pendingInsertBatch = 0;
                }
            }
            //finish off batches
            if (!pendingUpdateParams.isEmpty()) {
                int[] updates = updateStmt.executeBatch();
                for (int i = 0; i < updates.length; i++) {
                    if (updates[i] == 0) {
                        addBatch(insertStmt, pendingUpdateParams.get(i));
                        pendingInsertBatch++;
                    }
                }
            }
            if (pendingInsertBatch > 0) {
                insertStmt.executeBatch();
            }
        } catch (SQLException e) {
            throw new HormeException(e);
        } finally {
            //close em all
            try {
                updateStmt.close();
            } catch (Exception e) { }
            try {
                insertStmt.close();
            } catch (Exception e) { }
            try {
                conn.close();
            } catch (Exception e) { }
            try {
                jdoConn.close();
            } catch (Exception e) { }
            try {
                pm.close();
            } catch (Exception e) { }
        }
    }
    
    private void addBatch(PreparedStatement stmt, Object... params) throws SQLException {
        for (int i = 0; i < params.length; i++) {
            stmt.setObject(i + 1, params[i]);
        }
        stmt.addBatch();
    }
}
