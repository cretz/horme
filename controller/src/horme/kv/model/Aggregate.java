package horme.kv.model;


public class Aggregate {

    private String dimensionOneId;
    private AggregateDimension dimensionOneType;
    private String dimensionTwoId;
    private AggregateDimension dimensionTwoType;
    private AggregateDuration duration;
    private long rangeStart;
    private long rangeEnd;
    private Number count;
    private Number total;
    
    public Aggregate() {
    }
    
    public Aggregate(String dimensionOneId,
            AggregateDimension dimensionOneType, String dimensionTwoId,
            AggregateDimension dimensionTwoType, AggregateDuration duration,
            long rangeStart, long rangeEnd, Number count, Number total) {
        this.dimensionOneId = dimensionOneId;
        this.dimensionOneType = dimensionOneType;
        this.dimensionTwoId = dimensionTwoId;
        this.dimensionTwoType = dimensionTwoType;
        this.duration = duration;
        this.rangeStart = rangeStart;
        this.rangeEnd = rangeEnd;
        this.count = count;
        this.total = total;
    }

    public String getDimensionOneId() {
        return dimensionOneId;
    }
    
    public void setDimensionOneId(String dimensionOneId) {
        this.dimensionOneId = dimensionOneId;
    }
    
    public AggregateDimension getDimensionOneType() {
        return dimensionOneType;
    }
    
    public void setDimensionOneType(AggregateDimension dimensionOneType) {
        this.dimensionOneType = dimensionOneType;
    }
    
    public String getDimensionTwoId() {
        return dimensionTwoId;
    }
    
    public void setDimensionTwoId(String dimensionTwoId) {
        this.dimensionTwoId = dimensionTwoId;
    }
    
    public AggregateDimension getDimensionTwoType() {
        return dimensionTwoType;
    }
    
    public void setDimensionTwoType(AggregateDimension dimensionTwoType) {
        this.dimensionTwoType = dimensionTwoType;
    }

    public AggregateDuration getDuration() {
        return duration;
    }

    public void setDuration(AggregateDuration duration) {
        this.duration = duration;
    }

    public long getRangeStart() {
        return rangeStart;
    }

    public void setRangeStart(long rangeStart) {
        this.rangeStart = rangeStart;
    }

    public long getRangeEnd() {
        return rangeEnd;
    }

    public void setRangeEnd(long rangeEnd) {
        this.rangeEnd = rangeEnd;
    }

    public Number getCount() {
        return count;
    }
    
    public void setCount(Number count) {
        this.count = count;
    }
    
    public Number getTotal() {
        return total;
    }
    
    public void setTotal(Number total) {
        this.total = total;
    }
}
