package horme.kv.model;

public enum AggregateDuration {
    MILLISECOND(1),
    FIFTEEN_MINUTE(1000 * 60 * 15),
    DAY(1000 * 60 * 60 * 24);
    
    public final long MS_MULTIPLIER;
    
    private AggregateDuration(long msMultiplier) {
        MS_MULTIPLIER = msMultiplier;
    }
}
