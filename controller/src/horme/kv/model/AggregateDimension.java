package horme.kv.model;

public enum AggregateDimension {
    AGENT,
    AGENT_GROUP,
    AGENT_METRIC,
    AGENT_METRIC_GROUP,
    CONTROLLER_GROUP
}
