package horme.kv;

import horme.kv.model.Aggregate;
import horme.kv.model.AggregateDimension;
import horme.kv.model.AggregateDuration;
import horme.kv.model.MetricData;

import java.util.Collection;
import java.util.List;

public interface KeyValueStore {
    
    void addMetricData(List<MetricData> metricData);
    
    List<MetricData> getMetricData(List<String> metricIds, Long start, 
            Long end, int resultLimit);
    
    List<Aggregate> getAggregates(String dimensionOneId, AggregateDimension dimensionOneType,
            String dimensionTwoId, AggregateDimension dimensionTwoType,
            AggregateDuration duration, Long start, Long end, int resultLimit);
    
    void incrementAggregates(Collection<Aggregate> increments);
}
