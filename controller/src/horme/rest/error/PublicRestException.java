package horme.rest.error;

import org.restlet.data.Status;

import horme.HormeException;

@SuppressWarnings("serial")
public class PublicRestException extends HormeException {
    
    private final Status status;
    
    public PublicRestException(Status status, String message) {
        super(message);
        this.status = status;
    }
    
    public PublicRestException(Status status, String message, Throwable cause) {
        super(message, cause);
        this.status = status;
    }
    
    public Status getStatus() {
        return status;
    }
    
    public RestError toRestError() {
        return new RestError(getOriginalMessage(), getCode());
    }
}
