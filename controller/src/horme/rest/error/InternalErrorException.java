package horme.rest.error;

import org.restlet.data.Status;

@SuppressWarnings("serial")
public class InternalErrorException extends PublicRestException {

    public InternalErrorException(Throwable cause) {
        super(Status.SERVER_ERROR_INTERNAL, "Internal error", cause);
    }
}
