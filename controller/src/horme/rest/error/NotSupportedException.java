package horme.rest.error;

import org.restlet.data.Status;

@SuppressWarnings("serial")
public class NotSupportedException extends PublicRestException {

    public NotSupportedException() {
        super(Status.CLIENT_ERROR_METHOD_NOT_ALLOWED, "Method not supported");
    }

}
