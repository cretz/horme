package horme.rest.error;

import org.restlet.data.Status;

@SuppressWarnings("serial")
public class NotFoundException extends PublicRestException {

    public NotFoundException() {
        this("Item not found");
    }
    
    public NotFoundException(String message) {
        super(Status.CLIENT_ERROR_NOT_FOUND, message);
    }
}
