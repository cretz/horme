package horme.rest.error;

import org.restlet.data.Status;

@SuppressWarnings("serial")
public class BadRequestException extends PublicRestException {

    public BadRequestException(String message) {
        super(Status.CLIENT_ERROR_BAD_REQUEST, message);
    }

}
