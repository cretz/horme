package horme.rest.resource;

import horme.Main;
import horme.db.model.Agent;
import horme.db.model.Controller;
import horme.db.model.ControllerGroup;
import horme.rest.error.BadRequestException;
import horme.rest.error.NotFoundException;
import horme.rest.model.ControllerResult;
import horme.rest.model.ControllerUpdate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.JDOHelper;

public class ControllerResource extends DbResource<ControllerUpdate> {

    protected ControllerResource() {
        super(ControllerUpdate.class);
    }
    
    @Override
    protected Object doDbGet() {
        if (isReqAttrPresent("controllerId")) {
            return new ControllerResult(assertControllerById());
        } else if (isReqAttrPresent("agentId")) {
            return new ControllerResult(assertAgentById().getPrimaryController());
        }
        Collection<Controller> controllers = dataStore.getAll(Controller.class);
        List<ControllerResult> ret = new ArrayList<ControllerResult>(controllers.size());
        for (Controller controller : controllers) {
            ret.add(new ControllerResult(controller));
        }
        return ret;
    }
    
    @Override
    protected ControllerResult doDbPost(ControllerUpdate update) {
        return new ControllerResult(updateController(new Controller(), update, false));
    }
    
    @Override
    protected ControllerResult doDbPut(ControllerUpdate update) {
        return new ControllerResult(updateController(assertControllerById(), update, false));
    }
    
    @Override
    protected ControllerResult doDbPatch(ControllerUpdate update) {
        return new ControllerResult(updateController(assertControllerById(), update, true));
    }
    
    @Override
    protected void doDbDelete() {
        dataStore.delete(assertControllerById());
    }
    
    protected Controller assertControllerById() {
        Controller controller = dataStore.getByIdOrName(Controller.class,
                getReqAttrString("controllerId"));
        if (controller == null) {
            throw new NotFoundException("Controller ID '" + 
                    getReqAttrString("controllerId") + "' not found");
        }
        return controller;
    }
    
    private Controller updateController(Controller controller, ControllerUpdate update,
            boolean patch) {
        if (update.name != null) {
            controller.setName(update.name);
        }
        if (update.controllerGroupId != null) {
            controller.setControllerGroup(dataStore.getByIdOrName(ControllerGroup.class, 
                    update.controllerGroupId));
            if (controller.getControllerGroup() == null) {
                throw new BadRequestException("controllerGroupId '" + update.controllerGroupId + 
                        "' not found");
            }
        } else if (!patch) {
            controller.setControllerGroup(dataStore.getByIdOrName(Controller.class, 
                    getGlobal().getMyControllerId()).getControllerGroup());
        }
        if (update.host != null) {
            controller.setHost(update.host);
        } else if (!patch) {
            throw new BadRequestException("host required");
        }
        if (update.port != null) {
            controller.setPort(update.port);
        } else if (!patch) {
            controller.setPort(Main.DEFAULT_METRIC_LISTENER_PORT);
        }
        if (update.mac != null) {
            controller.setMac(update.mac);
        } else if (!patch) {
            throw new BadRequestException("mac required");
        }
        if (JDOHelper.isDirty(controller)) {
            for (Agent agent : controller.getControllerGroup().getAgents()) {
                agent.setPendingUpdate(true);
            }
        }
        return dataStore.persist(controller);
    }
}
