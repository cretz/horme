package horme.rest.resource;

import horme.db.model.Agent;
import horme.db.model.Controller;
import horme.db.model.ControllerGroup;
import horme.rest.error.BadRequestException;
import horme.rest.model.AgentResult;
import horme.rest.model.AgentUpdate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.jdo.JDOHelper;

import com.google.common.net.InetAddresses;

public class AgentResource extends DbResource<AgentUpdate> {
    
    public AgentResource() {
        super(AgentUpdate.class);
    }

    @Override
    protected Object doDbGet() {
        if (isReqAttrPresent("agentId")) {
            return new AgentResult(assertAgentById());
        }
        Collection<Agent> agents = dataStore.getAll(Agent.class);
        List<AgentResult> ret = new ArrayList<AgentResult>(agents.size());
        for (Agent agent : agents) {
            ret.add(new AgentResult(agent));
        }
        return ret;
    }
    
    @Override
    protected AgentResult doDbPost(AgentUpdate update) {
        return new AgentResult(updateAgent(update, new Agent()));
    }
    
    @Override
    protected AgentResult doDbPut(AgentUpdate update) {
        return new AgentResult(updateAgent(update, assertAgentById()));
    }
    
    @Override
    protected AgentResult doDbPatch(AgentUpdate update) {
        Agent agent = assertAgentById();
        if (update.primaryControllerId != null) {
            updatePrimaryController(update, agent);
        }
        if (update.controllerGroupId != null) {
            updateControllerGroup(update, agent, false);
        }
        //is primary controller in group?
        if (update.primaryControllerId != null || update.controllerGroupId != null) {
            if (!isPrimaryControllerInControllerGroup(agent)) {
                if (update.primaryControllerId != null) {
                    if (update.controllerGroupId != null) {
                        throw new BadRequestException("Primary controller not in controller group");
                    }
                    agent.setControllerGroup(agent.getPrimaryController().getControllerGroup());
                } else if (update.controllerGroupId != null) {
                    agent.setPrimaryController(getLeastUsedController(agent.getControllerGroup()));
                }
            }
        }
        updateName(agent, update);
        updateIpAddress(agent, update);
        if (update.heartbeat != null) {
            agent.setHeartbeat(update.heartbeat);
        }
        agent.setPendingUpdate(JDOHelper.isDirty(agent));
        return new AgentResult(dataStore.persist(agent));
    }
    
    @Override
    protected void doDbDelete() {
        dataStore.delete(assertAgentById());
    }
    
    private Controller getLeastUsedController(ControllerGroup group) {
        Controller min = null;
        int minAmount = Integer.MAX_VALUE;
        for (Controller controller : group.getControllers()) {
            if (controller.getPrimaryAgents().size() < minAmount) {
                minAmount = controller.getPrimaryAgents().size();
                min = controller;
            }
        }
        return min;
    }
    
    private void updatePrimaryController(AgentUpdate update, Agent agent) {
        if (update.primaryControllerId != null) {
            agent.setPrimaryController(dataStore.getByIdOrName(Controller.class, 
                    update.primaryControllerId));
            if (agent.getPrimaryController() == null) {
                throw new BadRequestException("primaryControllerId '" + 
                        update.primaryControllerId + "' not found");
            }
        }
    }
    
    private void updateControllerGroup(AgentUpdate update, Agent agent, boolean checkPrimary) {
        if (update.controllerGroupId != null) {
            if (checkPrimary && agent.getPrimaryController() != null) {
                throw new BadRequestException(
                        "Controller group and primary controller cannot both be present");
            }
            agent.setControllerGroup(dataStore.getByIdOrName(ControllerGroup.class, 
                    update.controllerGroupId));
            if (agent.getControllerGroup() == null) {
                throw new BadRequestException("controllerGroupId '" + 
                        update.controllerGroupId + "' not found");
            }
        }
    }
    
    private boolean isPrimaryControllerInControllerGroup(Agent agent) {
        for (Controller controller : agent.getControllerGroup().getControllers()) {
            if (controller.getId().equals(agent.getPrimaryController().getId())) {
                return true;
            }
        }
        return false;
    }
    
    private void updateName(Agent agent, AgentUpdate update) {
        //name
        if (update.name != null) {
            if (update.name.length() > 255) {
                throw new BadRequestException("Name cannot be more than 255 characters");
            }
            agent.setName(update.name);
        }
    }
    
    private void updateIpAddress(Agent agent, AgentUpdate update) {
        if (update.ipAddress != null) {
            if (!InetAddresses.isInetAddress(update.ipAddress)) {
                throw new BadRequestException("Invalid ipAddress: " + update.ipAddress);
            }
            agent.setIpAddress(update.ipAddress);
        }
    }
    
    private Agent updateAgent(AgentUpdate update, Agent agent) {
        updatePrimaryController(update, agent);
        updateControllerGroup(update, agent, true);
        //defaults for controller and controller group
        if (agent.getPrimaryController() == null) {
            if (agent.getControllerGroup() == null) {
                Controller myController = dataStore.getById(Controller.class, 
                        getGlobal().getMyControllerId());
                agent.setControllerGroup(myController.getControllerGroup());
            }
            agent.setPrimaryController(getLeastUsedController(agent.getControllerGroup()));
        } else if (agent.getControllerGroup() == null) {
            agent.setControllerGroup(agent.getPrimaryController().getControllerGroup());
        }
        updateName(agent, update);
        updateIpAddress(agent, update);
        //heartbeat
        if (update.heartbeat == null) {
            throw new BadRequestException("heartbeat required");
        }
        agent.setHeartbeat(update.heartbeat);
        agent.setPendingUpdate(JDOHelper.isDirty(agent));
        return dataStore.persist(agent);
    }
}
