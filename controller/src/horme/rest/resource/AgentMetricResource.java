package horme.rest.resource;

import horme.db.model.AgentMetric;
import horme.db.model.Metric;
import horme.db.model.MetricParameter;
import horme.rest.error.BadRequestException;
import horme.rest.error.NotFoundException;
import horme.rest.model.AgentMetricResult;
import horme.rest.model.AgentMetricUpdate;
import horme.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jdo.JDOHelper;

public class AgentMetricResource extends DbResource<AgentMetricUpdate> {
    
    public AgentMetricResource() {
        super(AgentMetricUpdate.class);
    }

    @Override
    protected Object doDbGet() {
        if (isReqAttrPresent("agentMetricId")) {
            return new AgentMetricResult(assertAgentMetricById());
        }
        Collection<AgentMetric> metrics;
        if (isReqAttrPresent("agentId")) {
            metrics = assertAgentById().getAgentMetrics();
        } else {
            metrics = dataStore.getAll(AgentMetric.class);
        }
        List<AgentMetricResult> ret = new ArrayList<AgentMetricResult>(metrics.size());
        for (AgentMetric metric : metrics) {
            ret.add(new AgentMetricResult(metric));
        }
        return ret;
    }
    
    @Override
    protected AgentMetricResult doDbPost(AgentMetricUpdate update) {
        return new AgentMetricResult(updateAgentMetric(new AgentMetric(), update, false));
    }
    
    @Override
    protected AgentMetricResult doDbPut(AgentMetricUpdate update) {
        return new AgentMetricResult(updateAgentMetric(assertAgentMetricById(), update, false));
    }
    
    @Override
    protected AgentMetricResult doDbPatch(AgentMetricUpdate update) {
        return new AgentMetricResult(updateAgentMetric(assertAgentMetricById(), update, true));
    }
    
    @Override
    protected void doDbDelete() {
        dataStore.delete(assertAgentMetricById());
    }
    
    protected AgentMetric assertAgentMetricById() {
        AgentMetric metric = dataStore.getByIdOrName(AgentMetric.class,
                getReqAttrString("agentMetricId"));
        if (metric == null) {
            throw new NotFoundException("Agent metric ID '" + 
                    getReqAttrString("agentMetricId") + "' not found");
        }
        return metric;
    }
    
    private AgentMetric updateAgentMetric(AgentMetric metric, AgentMetricUpdate update,
            boolean patch) {
        if (update.name != null) {
            metric.setName(update.name);
        }
        if (update.metricId != null) {
            metric.setMetric(dataStore.getByIdOrName(Metric.class, update.metricId));
            if (metric.getMetric() == null) {
                throw new BadRequestException("metricId '" + update.metricId + 
                        "' not found");
            }
        } else if (!patch) {
            throw new BadRequestException("metricId required");
        }
        if (update.haltOnFailure != null) {
            metric.setHaltOnFailure(update.haltOnFailure);
        } else if (!patch) {
            metric.setHaltOnFailure(true);
        }
        if (update.params != null) {
            metric.setParams(update.params);
        } else if (!patch) {
            metric.setParams(new HashMap<String, String>());
        }
        //validate/set params
        if (update.params != null || !patch) {
            validateAndSetDefaultParams(metric);
        }
        if (JDOHelper.isDirty(metric)) {
            metric.getAgent().setPendingUpdate(true);
        }
        return dataStore.persist(metric);
    }
    
    private void validateAndSetDefaultParams(AgentMetric metric) {
        //first, go through all metrics, check and set defaults
        Set<String> paramNames = new HashSet<String>(metric.getMetric().getParameters().size());
        for (MetricParameter param : metric.getMetric().getParameters()) {
            String value = metric.getParams().get(param.getName());
            if (value == null) {
                if (param.getDefaultValue() != null) {
                    //set default
                    metric.getParams().put(param.getName(), param.getDefaultValue());
                } else if (param.isRequired()) {
                    throw new BadRequestException("Parameter '" + 
                            param.getName() + "' is required");
                }
            } else {
                //check value
                if (!param.getDataType().isValid(value)) {
                    throw new BadRequestException("Parameter '" + param.getName() +
                            "' value '" + value + "' is invalid");
                }
            }
            paramNames.add(param.getName());
        }
        //now see if there are any params we don't recognize
        Set<String> badParams = new HashSet<String>(metric.getParams().keySet());
        badParams.removeAll(paramNames);
        if (!badParams.isEmpty()) {
            throw new BadRequestException("Invalid parameters: " +
                    StringUtils.commaDelimit(badParams));
        }
    }
}
