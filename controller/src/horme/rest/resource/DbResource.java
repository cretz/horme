package horme.rest.resource;

import horme.db.DataStore;
import horme.db.model.Agent;
import horme.rest.error.NotFoundException;
import horme.rest.error.NotSupportedException;
import horme.rest.model.RestUpdateModel;

import javax.jdo.Transaction;

import org.restlet.resource.ResourceException;

public abstract class DbResource<Update extends RestUpdateModel> extends BaseResource {

    protected DataStore dataStore;
    private Class<Update> updateClass;
    
    protected DbResource(Class<Update> updateClass) {
        super();
        this.updateClass = updateClass;
    }
        
    protected Agent assertAgentById() {
        Agent agent = dataStore.getByIdOrName(Agent.class, getReqAttrString("agentId"));
        if (agent == null) {
            throw new NotFoundException("Agent ID '" + 
                    getReqAttrString("agentId") + "' not found");
        }
        return agent;
    }
    
    @Override
    protected void doInit() throws ResourceException {
        super.doInit();
        dataStore = new DataStore(getGlobal().
                getPersistenceManagerFactory().getPersistenceManager());
    }
    
    @Override
    protected void doRelease() throws ResourceException {
        super.doRelease();
        if (dataStore != null) {
            dataStore.close();
        }
    }
    
    private String doDb(Method method, String string) {
        Transaction tx = dataStore.currentTransaction();
        try {
            Object result;
            Update update = null;
            if (string != null) {
                update = getGlobal().getGson().fromJson(string, updateClass);
            }
            tx.begin();
            switch (method) {
            case GET:
                result = doDbGet();
                break;
            case POST:
                result = doDbPost(update);
                break;
            case PUT:
                result = doDbPut(update);
                break;
            case PATCH:
                result = doDbPatch(update);
                break;
            case DELETE:
                doDbDelete();
                tx.commit();
                return null;
            default:
                throw new NotSupportedException();
            }
            tx.commit();
            if (result == null) {
                throw new NotFoundException();
            }
            return getGlobal().getGson().toJson(result);
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
        }
    }
    
    @Override
    protected String doGet() {
        return doDb(Method.GET, null);
    }
    
    @Override
    protected String doPost(String string) {
        return doDb(Method.POST, string);
    }
    
    @Override
    protected String doPut(String string) {
        return doDb(Method.PUT, string);
    }
    
    @Override
    protected String doPatch(String string) {
        return doDb(Method.PATCH, string);
    }
    
    @Override
    protected void doDelete() {
        doDb(Method.DELETE, null);
    }
    
    protected Object doDbGet() {
        throw new NotSupportedException();
    }
    
    protected Object doDbPost(Update update) {
        throw new NotSupportedException();
    }
    
    protected Object doDbPut(Update update) {
        throw new NotSupportedException();
    }
    
    protected Object doDbPatch(Update update) {
        throw new NotSupportedException();
    }
    
    protected void doDbDelete() {
        throw new NotSupportedException();
    }
    
    private static enum Method {
        GET, POST, PUT, PATCH, DELETE;
    }
}
