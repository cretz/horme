package horme.rest.resource;

import horme.GlobalState;
import horme.rest.ControllerRestApplication;
import horme.rest.Patch;
import horme.rest.error.InternalErrorException;
import horme.rest.error.PublicRestException;

import org.restlet.data.MediaType;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.Put;
import org.restlet.resource.ServerResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseResource extends ServerResource {
    
    //special rest logger
    protected static Logger logger = LoggerFactory.getLogger("RestService");
    
    public GlobalState getGlobal() {
        return ((ControllerRestApplication) getApplication()).getGlobal();
    }
    
    protected boolean isReqAttrPresent(String name) {
        return getRequestAttributes().containsKey(name);
    }
    
    protected String getReqAttrString(String name) {
        Object attr = getRequestAttributes().get(name);
        return attr == null ? null : attr.toString();
    }
    
    @Override
    protected void doCatch(Throwable throwable) {
        PublicRestException ex;
        if (throwable.getCause() instanceof PublicRestException) {
            ex = (PublicRestException) throwable.getCause();
            if (logger.isInfoEnabled()) {
                logger.info("Returning error in {}: {}", getClass().getSimpleName(), ex.getMessage());
            }
        } else {
            ex = new InternalErrorException(throwable);
            logger.error("Error in {}", getClass().getSimpleName(), ex);
        }
        setStatus(ex.getStatus(), ex.getMessage());
        getResponse().setEntity(getGlobal().getGson().toJson(
                ex.toRestError()), MediaType.APPLICATION_JSON);
    }
    
    @Get
    public final void handleGet() {
        String value = doGet();
        getResponse().setEntity(value, MediaType.APPLICATION_JSON);
    }
    
    @Post
    public final void handlePost(String string) {
        String value = doPost(string);
        getResponse().setEntity(value, MediaType.APPLICATION_JSON);
    }
    
    @Put
    public final void handlePut(String string) {
        String value = doPut(string);
        getResponse().setEntity(value, MediaType.APPLICATION_JSON);
    }
    
    @Patch
    public final void handlePatch(String string) {
        String value = doPatch(string);
        getResponse().setEntity(value, MediaType.APPLICATION_JSON);
    }
    
    @Delete
    public final void handleDelete() {
        doDelete();
    }

    protected abstract String doGet();
    protected abstract String doPost(String string);
    protected abstract String doPut(String string);
    protected abstract String doPatch(String string);
    protected abstract void doDelete();
}
