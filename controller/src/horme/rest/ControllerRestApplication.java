package horme.rest;

import horme.GlobalState;
import horme.rest.resource.AgentMetricResource;
import horme.rest.resource.AgentResource;
import horme.rest.resource.ControllerResource;

import org.restlet.Application;
import org.restlet.Restlet;
import org.restlet.routing.Router;

public class ControllerRestApplication extends Application {

    private final GlobalState global;
    
    public ControllerRestApplication(GlobalState global) {
        this.global = global;
    }
    
    public GlobalState getGlobal() {
        return global;
    }
    
    @Override
    public Restlet createInboundRoot() {
        Router router = new Router(getContext());
        
        //please keep in alphabetical order
        router.attach("/agent", AgentResource.class);
        router.attach("/agent/{agentId}", AgentResource.class);
        router.attach("/agent/{agentId}/agentMetric", AgentMetricResource.class);
        router.attach("/agent/{agentId}/primaryController", ControllerResource.class);
        router.attach("/agentMetric/{agentMetricId}", AgentMetricResource.class);
        router.attach("/controller", ControllerResource.class);
        router.attach("/controller/{controllerId}", ControllerResource.class);
        return router;
    }
}
