package horme.rest;

import org.restlet.Application;
import org.restlet.Component;
import org.restlet.data.Protocol;

import horme.GlobalState;

public class RestServer {

    private final GlobalState global;
    private Component component;
    
    public RestServer(GlobalState global) {
        this.global = global;
    }
    
    public void start() throws Exception {
        component = new Component();
        component.getServers().add(Protocol.HTTP, global.getConf().getRestService().getPort());
        Application application = new ControllerRestApplication(global);
        component.getDefaultHost().attach(application);
        component.start();
    }

    public void stop() throws Exception {
        if (component != null) {
            component.stop();
        }
    }
}
