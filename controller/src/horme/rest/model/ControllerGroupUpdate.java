package horme.rest.model;

public class ControllerGroupUpdate implements RestUpdateModel {

    public String name;
    public String host;
    public Integer port;
    public String mac;
    public String controllerGroupId;
}
