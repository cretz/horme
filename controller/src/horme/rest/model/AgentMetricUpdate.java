package horme.rest.model;

import java.util.Map;

public class AgentMetricUpdate implements RestUpdateModel {
    
    public String name;
    public String metricId;
    public Boolean haltOnFailure;
    public Map<String, String> params;
}
