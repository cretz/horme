package horme.rest.model;

import horme.db.model.Agent;

public class AgentReference {
    public String id;
    public String name;
    public String href;
    
    public AgentReference(Agent agent) {
        id = agent.getId();
        name = agent.getName();
        href = "/agent/" + id;
    }
}