package horme.rest.model;

import horme.db.model.Controller;

public class ControllerReference {
    public String id;
    public String name;
    public String href;
    
    public ControllerReference(Controller controller) {
        id = controller.getId();
        name = controller.getName();
        href = "/controller/" + id;
    }

}
