package horme.rest.model;

import horme.db.model.AgentMetric;

import java.util.Map;

public class AgentMetricResult {

    public String id;
    public String name;
    public String href;
    public AgentReference agent;
    public MetricReference metric;
    public boolean haltOnFailure;
    public Map<String, String> params;
    
    public AgentMetricResult(AgentMetric metric) {
        id = metric.getId();
        name = metric.getName();
        href = "/agentMetric/" + id;
        agent = new AgentReference(metric.getAgent());
        this.metric = new MetricReference(metric.getMetric());
        haltOnFailure = metric.isHaltOnFailure();
        params = metric.getParams();
    }
}
