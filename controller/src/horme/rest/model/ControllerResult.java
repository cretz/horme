package horme.rest.model;

import horme.db.model.Controller;

public class ControllerResult {

    public String id;
    public String name;
    public String href;
    public String host;
    public int port;
    public String mac;
    public ControllerGroupReference controllerGroup;
    
    public ControllerResult(Controller controller) {
        id = controller.getId();
        name = controller.getName();
        href = "/controller/" + id;
        host = controller.getHost();
        port = controller.getPort();
        mac = controller.getMac();
        controllerGroup = new ControllerGroupReference(controller.getControllerGroup());
    }
}
