package horme.rest.model;

public class AgentUpdate implements RestUpdateModel {

    public String name;
    public String ipAddress;
    public Integer heartbeat;
    public String primaryControllerId;
    public String controllerGroupId;
}
