package horme.rest.model;

import horme.db.model.ControllerGroup;

public class ControllerGroupReference {
    public String id;
    public String name;
    public String href;
    
    public ControllerGroupReference(ControllerGroup controllerGroup) {
        id = controllerGroup.getId();
        name = controllerGroup.getName();
        href = "/controllerGroup/" + id;
    }

}
