package horme.rest.model;

import horme.db.model.Metric;

public class MetricReference {
    public String id;
    public String name;
    public String href;
    
    public MetricReference(Metric metric) {
        id = metric.getId();
        name = metric.getName();
        href = "/metric/" + id;
    }
}