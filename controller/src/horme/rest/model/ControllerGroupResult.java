package horme.rest.model;

import horme.db.model.Agent;
import horme.db.model.Controller;
import horme.db.model.ControllerGroup;

import java.util.ArrayList;
import java.util.List;

public class ControllerGroupResult {

    public String id;
    public String name;
    public String href;
    public List<ControllerResult> controllers;
    public List<AgentReference> agents;
    
    public ControllerGroupResult(ControllerGroup group) {
        id = group.getId();
        name = group.getName();
        href = "/controllerGroup/" + id;
        controllers = new ArrayList<ControllerResult>(group.getControllers().size());
        for (Controller controller : group.getControllers()) {
            controllers.add(new ControllerResult(controller));
        }
        agents = new ArrayList<AgentReference>(group.getAgents().size());
        for (Agent agent : group.getAgents()) {
            agents.add(new AgentReference(agent));
        }
    }
    
    public static class ControllerResult {

        public String id;
        public String name;
        public String href;
        public String host;
        public int port;
        public String mac;
        
        public ControllerResult(Controller controller) {
            id = controller.getId();
            name = controller.getName();
            href = "/controller/" + id;
            host = controller.getHost();
            port = controller.getPort();
            mac = controller.getMac();
        }
    }

}
