package horme.rest.model;

import horme.db.model.Agent;
import horme.db.model.AgentMetric;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AgentResult {
    
    public String id;
    public String name;
    public String href;
    public String ipAddress;
    public long heartbeat;
    public ControllerReference primaryController;
    public ControllerGroupReference controllerGroup;
    public List<AgentMetricResult> agentMetrics;
    
    public AgentResult(Agent agent) {
        id = agent.getId();
        name = agent.getName();
        href = "/agent/" + id;
        ipAddress = agent.getIpAddress();
        heartbeat = agent.getHeartbeat();
        primaryController = new ControllerReference(agent.getPrimaryController());
        controllerGroup = new ControllerGroupReference(agent.getControllerGroup());
        agentMetrics = new ArrayList<AgentMetricResult>(agent.getAgentMetrics().size());
        for (AgentMetric metric : agent.getAgentMetrics()) {
            agentMetrics.add(new AgentMetricResult(metric));
        }
    }
    
    public static class AgentMetricResult {

        public String id;
        public String name;
        public String href;
        public MetricReference metric;
        public boolean haltOnFailure;
        public Map<String, String> params;
        
        public AgentMetricResult(AgentMetric metric) {
            id = metric.getId();
            name = metric.getName();
            href = "/agentMetric/" + id;
            this.metric = new MetricReference(metric.getMetric());
            haltOnFailure = metric.isHaltOnFailure();
            params = metric.getParams();
        }
    }
}
