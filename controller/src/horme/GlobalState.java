package horme;

import horme.conf.Configuration;
import horme.kv.KeyValueStore;
import horme.util.JsonUtils;

import javax.jdo.PersistenceManagerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GlobalState {
    
    static Gson newGson() {
        return new GsonBuilder().
            serializeNulls().
            registerTypeHierarchyAdapter(Enum.class, new JsonUtils.EnumTypeAdapter()).
            create();
    }

    private Configuration conf;
    private PersistenceManagerFactory persistenceManagerFactory;
    private final Gson gson;
    private KeyValueStore keyValueStore;
    private String myControllerId;
    
    public GlobalState() {
        gson = newGson();
    }
    
    public Configuration getConf() {
        return conf;
    }
    
    public void setConf(Configuration conf) {
        this.conf = conf;
    }
    
    public PersistenceManagerFactory getPersistenceManagerFactory() {
        return persistenceManagerFactory;
    }
    
    public void setPersistenceManagerFactory(PersistenceManagerFactory persistenceManagerFactory) {
        this.persistenceManagerFactory = persistenceManagerFactory;
    }
    
    public Gson getGson() {
        return gson;
    }
    
    public KeyValueStore getKeyValueStore() {
        return keyValueStore;
    }
    
    public void setKeyValueStore(KeyValueStore keyValueStore) {
        this.keyValueStore = keyValueStore;
    }
    
    public String getMyControllerId() {
        return myControllerId;
    }
    
    public void setMyControllerId(String myControllerId) {
        this.myControllerId = myControllerId;
    }
}
