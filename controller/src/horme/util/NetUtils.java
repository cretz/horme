package horme.util;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;

public class NetUtils {

    public static String getMacAddress() throws SocketException, UnknownHostException {
        byte[] mac = NetworkInterface.getByInetAddress(InetAddress.
                getLocalHost()).getHardwareAddress();
        StringBuilder sb = new StringBuilder(mac.length * 3);
        for (int i = 0; i < mac.length; i++) {
            sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));        
        }
        return sb.toString();
    }
    
    private NetUtils() {
    }
}
