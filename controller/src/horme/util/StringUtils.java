package horme.util;

import com.google.common.base.Joiner;

public class StringUtils {

    private static final Joiner commaJoin = Joiner.on(',');

    public static String commaDelimit(Iterable<?> iterable) {
        return commaJoin.join(iterable);
    }
    
    public static String commaDelimit(Object[] array) {
        return commaJoin.join(array);
    }
    
    private StringUtils() {
    }
}
