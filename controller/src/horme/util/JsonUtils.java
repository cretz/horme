package horme.util;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class JsonUtils {

    private JsonUtils() {
    }
    
    public static class EnumTypeAdapter 
            implements JsonDeserializer<Enum<?>>, JsonSerializer<Enum<?>> {

        @Override
        public JsonElement serialize(Enum<?> src, Type typeOfSrc, JsonSerializationContext ctx) {
            //lowercase, underscores become dashes
            return new JsonPrimitive(src.name().toLowerCase().replace('_', '-'));
        }

        @Override
        @SuppressWarnings({ "rawtypes", "unchecked" })
        public Enum<?> deserialize(JsonElement json, 
                Type type, JsonDeserializationContext ctx) {
            //uppercase, dashes become underscores
            return Enum.valueOf((Class) type, json.getAsString().toUpperCase().replace('-', '_'));
        }
        
    }
}
