package horme.conf;

import horme.HormeException;

import java.util.Properties;

public enum DatabaseVendor {
    POSTGRES {
        @Override
        public void applyProperties(Database database, Properties properties) {
            //validate and create URL
            String url = "jdbc:postgresql:";
            if (database.getHost() != null) {
                url += "//" + database.getHost();
                if (database.getPort() != null) {
                    url += ":" + database.getPort();
                }
                url += "/";
            } else if (database.getPort() != null) {
                throw new HormeException("Host required with port when using PostgreSQL");
            }
            if (database.getDatabase() == null) {
                throw new HormeException("Database required when using PostgreSQL");
            }
            url += database.getDatabase();
            
            properties.setProperty("datanucleus.ConnectionDriverName", "org.postgresql.Driver");
            properties.setProperty("datanucleus.ConnectionURL", url);
            properties.setProperty("datanucleus.ConnectionUserName", database.getUsername());
            properties.setProperty("datanucleus.ConnectionPassword", database.getPassword());
            properties.setProperty("datanucleus.validateSchema", "false");
            properties.setProperty("datanucleus.validateTables", "false");
            properties.setProperty("datanucleus.identifier.case", "PreserveCase");
        }
    },
    H2_IN_MEMORY {
        @Override
        public void applyProperties(Database database, Properties properties) {
            properties.setProperty("datanucleus.ConnectionDriverName", "org.h2.Driver");
            properties.setProperty("datanucleus.ConnectionURL", "jdbc:h2:mem:horme");
            properties.setProperty("datanucleus.autoCreateSchema", "true");
        }
    }
    ;
    
    public abstract void applyProperties(Database database, Properties properties);
}
