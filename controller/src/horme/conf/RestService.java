package horme.conf;

public class RestService {

    private Integer port;
    
    public Integer getPort() {
        return port;
    }
    
    public void setPort(Integer port) {
        this.port = port;
    }
}
