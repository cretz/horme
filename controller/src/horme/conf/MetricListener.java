package horme.conf;

public class MetricListener {

    private Integer port;
    
    public Integer getPort() {
        return port;
    }
    
    public void setPort(Integer port) {
        this.port = port;
    }
}
