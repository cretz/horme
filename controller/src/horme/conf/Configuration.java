package horme.conf;

public class Configuration {

    private MetricListener metricListener;
    private RestService restService;
    private Database database;
    
    public MetricListener getMetricListener() {
        return metricListener;
    }
    
    public void setMetricListener(MetricListener metricListener) {
        this.metricListener = metricListener;
    }
    
    public RestService getRestService() {
        return restService;
    }
    
    public void setRestService(RestService restService) {
        this.restService = restService;
    }
    
    public Database getDatabase() {
        return database;
    }
    
    public void setDatabase(Database database) {
        this.database = database;
    }
}
