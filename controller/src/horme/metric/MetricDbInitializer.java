package horme.metric;

import horme.db.DataStore;
import horme.db.model.Metric;
import horme.db.model.MetricPackage;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;

import com.google.common.io.Closeables;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;

public class MetricDbInitializer {

    public void persistAllPackages(Gson gson, DataStore dataStore) {        
        //load package list
        Reader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(
                    getClass().getResourceAsStream("packages.json")));
            JsonElement object = gson.fromJson(reader, JsonElement.class);
            JsonArray array = object.getAsJsonObject().get("packages").getAsJsonArray();
            for (int i = 0; i < array.size(); i++) {
                persistPackage(gson, dataStore, array.get(i).getAsString());
            }
        } finally {
            Closeables.closeQuietly(reader);
        }
    }

    private void persistPackage(Gson gson, DataStore dataStore, String name) {
        Reader reader = null;
        try {
            reader = new InputStreamReader(getClass().getResourceAsStream(
                    "packages/" + name + ".json"));
            MetricPackage pkg = gson.fromJson(reader, MetricPackage.class);
            //loop through the metrics and set the pkg
            for (Metric metric : pkg.getMetrics()) {
                metric.setMetricPackage(pkg);
            }
            dataStore.persist(pkg);
        } finally {
            Closeables.closeQuietly(reader);
        }
    }
}
