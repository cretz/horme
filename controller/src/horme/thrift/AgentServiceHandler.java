package horme.thrift;

import horme.GlobalState;
import horme.HormeException;
import horme.db.model.Agent;
import horme.db.model.AgentGroup;
import horme.db.model.AgentMetric;
import horme.db.model.AgentMetricGroup;
import horme.db.model.Controller;
import horme.kv.model.Aggregate;
import horme.kv.model.AggregateDimension;
import horme.kv.model.AggregateDuration;
import horme.thrift.agent.AgentConfig;
import horme.thrift.agent.AgentService;
import horme.thrift.agent.AgentUpdate;
import horme.thrift.agent.ControllerConfig;
import horme.thrift.agent.MetricConfig;
import horme.thrift.agent.MetricData;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jdo.FetchGroup;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.jdo.Query;
import javax.jdo.Transaction;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AgentServiceHandler implements AgentService.Iface {

    private static Logger logger = LoggerFactory.getLogger("MetricListener");
    
    private final GlobalState global;
    
    public AgentServiceHandler(GlobalState global) {
        this.global = global;
        PersistenceManagerFactory pmf = global.getPersistenceManagerFactory();
        //let's go ahead and add the AgentConfig fetch plan
        FetchGroup fg = pmf.getFetchGroup(Agent.class, "AgentConfig");
        fg.addMembers(
                "id",
                "name",
                "heartbeat",
                "primaryController.id",
                "controllers.id",
                "controllers.host",
                "controllers.port",
                "agentMetrics.id",
                "agentMetrics.metric.name",
                "agentMetrics.executionFrequency",
                "agentMetrics.haltOnFailure",
                "agentMetrics.params"
            );
        //add the metric dimension fetch plan
    }
    
    @Override
    public AgentConfig getConfig(String agentId) throws TException {
        logger.info("Agent {} requested config", agentId);
        return loadConfig(agentId, false);
    }
    
    private AgentConfig loadConfig(String agentId, boolean requirePending) {
        PersistenceManager pm = global.getPersistenceManagerFactory().getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        boolean agentValid = false;
        Agent agent = null;
        try {
            //query
            tx.begin();
            Query query = pm.newQuery(Agent.class);
            query.declareParameters("String agentId");
            if (requirePending) {
                query.setFilter("id == agentId && pendingUpdate");
            } else {
                query.setFilter("id == agentId");
            }
            query.setUnique(true);
            query.getFetchPlan().addGroup("AgentConfig");
            agent = (Agent) query.execute();
            agentValid = agent != null && agent.getPrimaryController() != null && 
                    agent.getControllerGroup() != null && !agent.getAgentMetrics().isEmpty();
            if (agentValid) {
                agent.setPendingUpdate(false);
            }
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            pm.close();
        }    
        //convert
        if (agentValid) {
            return null;
        }
        AgentConfig config = new AgentConfig();
        config.setId(agentId);
        config.setHeartbeat(agent.getHeartbeat());
        config.setPrimaryControllerId(agent.getPrimaryController().getId());
        for (Controller controller : agent.getControllerGroup().getControllers()) {
            config.putToControllers(controller.getId(), new ControllerConfig(
                    controller.getId(), controller.getHost(), controller.getPort()));
        }
        for (AgentMetric agentMetric : agent.getAgentMetrics()) {
            MetricConfig metric = new MetricConfig();
            metric.setId(agentMetric.getId());
            metric.setName(agentMetric.getMetric().getName());
            metric.setExecutionFrequency(agentMetric.getExecutionFrequency());
            metric.setHaltOnFailure(agentMetric.isHaltOnFailure());
            //copy for fear of the strange map JDO returns
            metric.setParams(new HashMap<String, String>(agentMetric.getParams()));
            config.putToMetrics(metric.getId(), metric);
        }
        return config;
    }
    
    private AgentUpdate checkAndReturnUpdate(String agentId) {
        AgentUpdate update = new AgentUpdate();
        update.setConfig(loadConfig(agentId, true));
        if (update.isSetConfig()) {
            logger.info("Sending agent {} an update", agentId);
        }
        return update;
    }

    @Override
    public AgentUpdate ping(String agentId) throws TException {
        logger.info("Agent {} pinged", agentId);
        return checkAndReturnUpdate(agentId);
    }

    @Override
    public AgentUpdate postMetrics(String agentId, List<MetricData> metrics) throws TException {
        //TODO: speed this up or defer it
        if (logger.isInfoEnabled()) {
            logger.info("Agent {} pushed {} metrics", agentId, metrics.size());
        }
        PersistenceManager pm = global.getPersistenceManagerFactory().getPersistenceManager();
        Transaction tx = pm.currentTransaction();
        Agent agent = null;
        try {
            tx.begin();
            //grab agent
            Query query = pm.newQuery(Agent.class);
            query.getFetchPlan().addGroup("AgentDimension");
            query.declareParameters("String agentId, String[] agentMetricIds");
            query.setFilter("id == agentId && agentMetricIds.contains(agentMetrics.id)");
            String[] agentMetricIds = new String[metrics.size()];
            for (int i = 0; i < metrics.size(); i++) {
                agentMetricIds[i] = metrics.get(i).getId();
            }
            query.setUnique(true);
            agent = (Agent) query.execute(agentId, agentMetricIds);
            tx.commit();
        } finally {
            if (tx.isActive()) {
                tx.rollback();
            }
            pm.close();
        }
        //map metrics
        Map<String, AgentMetric> agentMetrics = new HashMap<String, AgentMetric>(
                agent.getAgentMetrics().size());
        for (AgentMetric agentMetric : agent.getAgentMetrics()) {
            agentMetrics.put(agentMetric.getId(), agentMetric);
        }
        //create all possible aggregates
        List<Aggregate> aggs = new ArrayList<Aggregate>();
        for (MetricData metric : metrics) {
            AgentMetric agentMetric = agentMetrics.get(metric.getId());
            if (agentMetric == null) {
                continue;
            }
            //normalize range values
            //TODO: make durations dynamic?
            long fifteenMinuteStart = metric.getTime() - 
                    (metric.getTime() % AggregateDuration.FIFTEEN_MINUTE.MS_MULTIPLIER);
            long fifteenMinuteEnd = fifteenMinuteStart + 
                    AggregateDuration.FIFTEEN_MINUTE.MS_MULTIPLIER;
            long dayStart = metric.getTime() - 
                    (metric.getTime() % AggregateDuration.DAY.MS_MULTIPLIER);
            long dayEnd = dayStart + AggregateDuration.DAY.MS_MULTIPLIER;
            Number total = getMetricIncrement(agentMetric, metric);
            for (AggregateDimension dimension : AggregateDimension.values()) {
                for (String dimensionId : getDimensionIds(agent, agentMetric, dimension)) {
                    for (AggregateDimension otherDimension : AggregateDimension.values()) {
                        if (otherDimension == dimension) {
                            continue;
                        }
                        for (String otherDimensionId : getDimensionIds(agent, agentMetric, 
                                otherDimension)) {
                            //add the three dimensions
                            aggs.add(new Aggregate(dimensionId, dimension, 
                                    otherDimensionId, otherDimension, 
                                    AggregateDuration.MILLISECOND, 
                                    metric.getTime(), metric.getTime(), 1, total));
                            aggs.add(new Aggregate(dimensionId, dimension, 
                                    otherDimensionId, otherDimension, 
                                    AggregateDuration.FIFTEEN_MINUTE, 
                                    fifteenMinuteStart, fifteenMinuteEnd, 1, total));
                            aggs.add(new Aggregate(dimensionId, dimension, 
                                    otherDimensionId, otherDimension, 
                                    AggregateDuration.DAY, 
                                    dayStart, dayEnd, 1, total));
                        }
                    }
                }
            }
        }
        //add to key value store
        global.getKeyValueStore().incrementAggregates(aggs);
        //TODO: notifications and better aggregations
        //check and return
        return checkAndReturnUpdate(agentId);
    }
    
    private Number getMetricIncrement(AgentMetric agentMetric, MetricData metricData) {
        switch (agentMetric.getMetric().getDataType()) {
        case BOOL:
            return metricData.isDataBool() ? 1 : 0;
        case BYTE:
            return metricData.isSetDataByte() ? 1 : 0;
        case DOUBLE:
            return metricData.getDataDouble();
        case I16:
            return metricData.getDataI16();
        case I32:
            return metricData.getDataI32();
        case I64:
            return metricData.getDataI64();
        case LIST:
            return metricData.getDataListSize();
        case MAP:
            return metricData.getDataMapSize();
        case STRING:
            return metricData.isSetDataString() ? 1 : 0;
        case UI64:
            return metricData.isSetDataString() ? new BigInteger(
                    metricData.getDataString()) : 0;
        default:
            throw new HormeException("Unrecognized data type: " +
                    agentMetric.getMetric().getDataType());
        }
    }

    private Collection<String> getDimensionIds(Agent agent, AgentMetric metric, 
            AggregateDimension dimension) {
        switch (dimension) {
        case AGENT:
            if (agent == null) {
                return Collections.emptyList();
            }
            return Collections.singleton(agent.getId());
        case AGENT_GROUP:
            if (agent == null) {
                return Collections.emptyList();
            }
            List<String> groupIds = new ArrayList<String>(agent.getAgentGroups().size());
            for (AgentGroup group : agent.getAgentGroups()) {
                groupIds.add(group.getId());
            }
            return groupIds;
        case AGENT_METRIC:
            if (metric == null) {
                return Collections.emptyList();
            }
            return Collections.singleton(metric.getId());
        case AGENT_METRIC_GROUP:
            if (metric == null) {
                return Collections.emptyList();
            }
            groupIds = new ArrayList<String>(metric.getAgentMetricGroups().size());
            for (AgentMetricGroup group : metric.getAgentMetricGroups()) {
                groupIds.add(group.getId());
            }
            return groupIds;
        case CONTROLLER_GROUP:
            return Collections.singleton(global.getMyControllerId());
        default:
            throw new HormeException("Unknown dimension: " + dimension);
        }
    }
}
