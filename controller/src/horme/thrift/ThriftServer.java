package horme.thrift;

import horme.GlobalState;
import horme.thrift.agent.AgentService.Processor;

import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TServer.Args;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;
import org.apache.thrift.transport.TTransportException;

public class ThriftServer {

    private GlobalState global;
    private TServer server;
    
    public ThriftServer(GlobalState global) {
        this.global = global;
    }
    
    public void start() throws TTransportException {
        AgentServiceHandler handler = new AgentServiceHandler(global);
        Processor<AgentServiceHandler> processor = new Processor<AgentServiceHandler>(handler);
        TServerTransport transport = new TServerSocket(global.getConf().getMetricListener().getPort());
        server = new TSimpleServer(new Args(transport).processor(processor));
        server.serve();
    }

    public void stop() {
        if (server != null) {
            server.stop();
        }
    }
}
