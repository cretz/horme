package horme;

import java.util.Random;

@SuppressWarnings("serial")
public class HormeException extends RuntimeException {
    
    private static ThreadLocal<Random> random = new ThreadLocal<Random>() {
        @Override
        protected Random initialValue() {
            return new Random();
        }
    };

    private static String createCode() {
        return Long.toHexString(System.currentTimeMillis()) + 
                Integer.toHexString(random.get().nextInt());
    }
    
    private final String code = createCode();
    
    public HormeException(String message) {
        super(message);
    }
    
    public HormeException(Throwable cause) {
        super(cause);
    }
    
    public HormeException(String message, Throwable cause) {
        super(message, cause);
    }
    
    @Override
    public String getMessage() {
        return super.getMessage() + "[" + code + "]";
    }
    
    public String getOriginalMessage() {
        return super.getMessage();
    }
    
    public String getCode() {
        return code;
    }
}
