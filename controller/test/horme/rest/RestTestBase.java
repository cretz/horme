package horme.rest;

import horme.MainTestBase;

import org.junit.Before;

public abstract class RestTestBase extends MainTestBase {
    
    @Before
    public void setUp() throws Exception {
        startMain(true, false);
    }
    
}
