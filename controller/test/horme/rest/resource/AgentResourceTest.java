package horme.rest.resource;

import junit.framework.Assert;
import horme.rest.model.AgentUpdate;

import org.junit.Test;
import org.restlet.data.Method;
import org.restlet.data.Status;

import com.google.common.base.Strings;
import com.google.gson.JsonElement;

public class AgentResourceTest extends ResourceTestBase {

    @Test
    public void testSimple() {
        AgentUpdate agent = new AgentUpdate();
        agent.name = "test";
        agent.ipAddress = "1.2.3.4";
        agent.heartbeat = 50000;
        //name too long
        agent.name = Strings.repeat("meh", 200);
        assertStatus(Method.POST, "/agent", Status.CLIENT_ERROR_BAD_REQUEST, agent);
        agent.name = "test";
        //TODO: more validators
        //good
        JsonElement result = assertResult(Method.POST, "/agent", agent);
        //it has an ID, right?
        String id = result.getAsJsonObject().get("id").getAsString();
        //now get all and check ID
        result = assertResult(Method.GET, "/agent");
        Assert.assertEquals(1, result.getAsJsonArray().size());
        Assert.assertEquals(id, result.getAsJsonArray().get(0).getAsJsonObject()
                .get("id").getAsString());
        //now get the specific agent and check ID
        result = assertResult(Method.GET, "/agent/" + id);
        Assert.assertEquals(id, result.getAsJsonObject().get("id").getAsString());
        //delete it
        assertResult(Method.DELETE, "/agent/" + id);
        //no longer there
        result = assertResult(Method.GET, "/agent");
        Assert.assertEquals(0, result.getAsJsonArray().size());
    }
}
