package horme.rest.resource;

import horme.rest.RestTestBase;

import org.junit.Assert;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Status;
import org.restlet.resource.ClientResource;

import com.google.gson.JsonElement;

public class ResourceTestBase extends RestTestBase {

    protected JsonElement assertResult(Method method, String path) {
        return assertResult(method, path, null);
    }
    
    protected JsonElement assertResult(Method method, String path, Object toSend) {
        ClientResource resource = new ClientResource(method, 
                "http://localhost:" + main.getGlobal().getConf().getRestService().
                getPort() + path);
        if (toSend != null) {
            resource.getRequest().setEntity(main.getGlobal().getGson().toJson(toSend), 
                    MediaType.APPLICATION_JSON);
        }
        resource.handle();
        Assert.assertEquals(Status.SUCCESS_OK, resource.getStatus());
        if (resource.getResponse().getEntity() == null) {
            return null;
        }
        return main.getGlobal().getGson().fromJson(resource.getResponse().
                getEntityAsText(), JsonElement.class);
    }
    
    protected JsonElement assertStatus(Method method, String path, Status status, Object toSend) {
        ClientResource resource = new ClientResource(method, 
                "http://localhost:" + main.getGlobal().getConf().getRestService().
                getPort() + path);
        if (toSend != null) {
            resource.getRequest().setEntity(main.getGlobal().getGson().toJson(toSend), 
                    MediaType.APPLICATION_JSON);
        }
        resource.handle();
        Assert.assertEquals(status, resource.getStatus());
        return main.getGlobal().getGson().fromJson(resource.getResponse().
                getEntityAsText(), JsonElement.class);
    }
}
