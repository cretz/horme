package horme.metric;

import horme.MainTestBase;
import horme.db.model.Metric;
import horme.db.model.MetricPackage;

import java.io.InputStreamReader;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class MetricDbInitializerTest extends MainTestBase {

    @Before
    public void setUp() throws Exception {
        //start up DB
        startMain(false, false);
    }
    
    @Test
    public void testPersistAllPackages() throws Exception {
        //these were created as part of the init
        //now let's grab that system package in JSON
        JsonObject object = main.getGlobal().getGson().fromJson(
                new InputStreamReader(getClass().getResourceAsStream("packages/system.json")),
                JsonElement.class).getAsJsonObject();
        dataStore = createDataStoreInTransaction();
        //check package info
        MetricPackage pkg = dataStore.getByIdOrName(MetricPackage.class, 
                object.get("name").getAsString());
        Assert.assertNotNull(pkg);
        Assert.assertNotNull(pkg.getId());
        Assert.assertEquals(object.get("name").getAsString(), pkg.getName());
        Assert.assertEquals(object.get("friendlyName").getAsString(), pkg.getFriendlyName());
        Assert.assertEquals(object.get("summary").getAsString(), pkg.getSummary());
        JsonArray metrics = object.get("metrics").getAsJsonArray();
        Assert.assertEquals(metrics.size(), pkg.getMetrics().size());
        for (int i = 0; i < metrics.size(); i++) {
            JsonObject expected = metrics.get(i).getAsJsonObject();
            Assert.assertNotNull(dataStore.getByIdOrName(Metric.class,
                    expected.get("name").getAsString()));
            //TODO: check more
        }
        closeDataStore(true);
    }
}
