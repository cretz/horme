package horme;

import horme.conf.Configuration;
import horme.conf.Database;
import horme.conf.DatabaseVendor;
import horme.conf.MetricListener;
import horme.conf.RestService;
import horme.db.DataStore;

import java.sql.Connection;
import java.sql.Statement;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.jdo.datastore.JDOConnection;

import org.junit.After;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MainTestBase {
    
    @SuppressWarnings("unused")
    private static final Logger logger = LoggerFactory.getLogger(MainTestBase.class);
    
    protected Main main;
    protected DataStore dataStore;
    protected Transaction currentTransaction;

    protected void startMain(boolean rest, boolean thrift) throws Exception {
        Configuration conf = new Configuration();
        conf.setDatabase(new Database());
        conf.getDatabase().setVendor(DatabaseVendor.H2_IN_MEMORY);
        if (rest) {
            conf.setRestService(new RestService());
            conf.getRestService().setPort(Main.DEFAULT_REST_SERVICE_PORT);
        }
        if (thrift) {
            conf.setMetricListener(new MetricListener());
            conf.getMetricListener().setPort(Main.DEFAULT_METRIC_LISTENER_PORT);
        }
        main = new Main(conf, true);
        main.start();
        Assert.assertNotNull(main.getGlobal().getMyControllerId());
    }
    
    protected DataStore createDataStoreInTransaction() {
        dataStore = new DataStore(main.getGlobal().getPersistenceManagerFactory().
                getPersistenceManager());
        currentTransaction = dataStore.currentTransaction();
        currentTransaction.begin();
        return dataStore;
    }
    
    protected void closeDataStore(boolean commit) {
        if (dataStore != null) {
            if (currentTransaction != null && currentTransaction.isActive()) {
                if (commit) {
                    currentTransaction.commit();
                } else {
                    currentTransaction.rollback();
                }
            }
            currentTransaction = null;
            dataStore.close();
            dataStore = null;
        }
    }
    
    @After
    public void tearDown() {
        closeDataStore(false);
        if (main != null) {
            //delete all objects
            PersistenceManager pm = main.getGlobal().
                    getPersistenceManagerFactory().getPersistenceManager();
            JDOConnection jdoConn = pm.getDataStoreConnection();
            Connection conn = (Connection) jdoConn.getNativeConnection();
            Statement stmt = null;
            try {
                stmt = conn.createStatement();
                stmt.execute("DROP ALL OBJECTS");
            } catch (Exception e) {
                throw new RuntimeException(e);
            } finally {
                try {
                    stmt.close();
                } catch (Exception e) { }
                try {
                    conn.close();
                } catch (Exception e) { }
                try {
                    jdoConn.close();
                } catch (Exception e) { }
                try {
                    pm.close();
                } catch (Exception e) { }
            }
            //stop server
            main.stop();
            main = null;
        }
    }
}
