# REST API

## Agent

GET /agent
----------
Get all agents

GET /agent/:agentId
-------------------
Get agent by ID or name

POST /agent, PUT /agent/:agentId, PATCH /agent/:agentId
-------------------------------------------------------
Create agent (POST), update entire agent (PUT), or partially update an agent (PUT). Sent as a JSON object in format:

* name - optional string

  The desired name of the agent for reference purposes. The maximum length is 255 characters. It must be unique within the controller group.

* ipAddress - optional string

  The IP address of the agent. This can be an IPv4 address or an IPv6 address (in expanded or collapsed form). This is only required to enable easy bootstrapping from the agent. Otherwise, it is populated the first time the agent checks in.

* heartbeat - optional integer

  The frequency in milliseconds this agent communicates with the controller. If it is not specified, it is defaulted to 30000 (30 seconds).

* primaryControllerId - optional string

  The ID or name of the primary controller to use for this agent. If a primary controller is not specified, it is uses the group of this controller to determine the most underused controller.

* controllerGroupId - optional string

  The ID or name of the controller group to assign to this agent. This is not allowed if a primary controller is provided. If neither this nor the primary controller is provided, it is assumed to belong to this controller.

DELETE /agent/:agentId
----------------------
Delete an agent by its ID or name

## Agent Metric

GET /agent/:agentId/agentMetric
-------------------------------
Get all agent metrics for an agent by the agent's ID or name

GET /agentMetric
----------------
Get all known agent metrics

GET /agentMetric/:agentMetricId
-------------------------
Get an agent metric by its ID or name

POST /agent/:agentId/agentMetric, PUT /agentMetric/:agentMetricId, PATCH /agentMetric/:agentMetricId
----------------------------------------------------------------------------------------------------
Create agent metric (POST), update entire agent metric (PUT), or partially update an agent metric (PUT). Sent as a JSON object in format:

* name - optional string

  The desired name of the agent metric for reference purposes. The maximum length is 255 characters.

* metricId - optional string
  
  The ID or name of the core metric being used. At least one of the two must be provided.

* haltOnFailure - optional boolean

  Whether or not to stop polling for this metric if it is unable to be obtained. The default value is true.

* params - optional object

  Object of string keys and values for the parameters. This is only required if the specific metric requires it.

## Controller

GET /agent/:agentId/primaryController
-------------------------------------
Get the primary controller for an agent by the agent's ID or name

GET /controller
---------------
Get all known controllers

GET /controller/:controllerId
-----------------------------
Get a controller by its ID or name

POST /controller, PUT /controller/:controllerId, PATCH /controller/:controllerId
----------------------------------------------------------------------------------------------------
Create controller (POST), update an entire controller (PUT), or partially update a controller (PUT). Sent as a JSON object in format:

* name - optional string

  The desired name of the controller for reference purposes. The maximum length is 255 characters.

* host - required string
  
  The host of the controller

* port - optional integer

  The port for the metric listener. If not provided, this defaults to 8710.

* mac - required string

  The mac address of the controller in canonical (i.e. dashed) form.

* controllerGroupId - optional string

  The ID or name of the controller group to assign this controller to. If not present, it will assign to the group the controller accepting the call is on.