module horme.pusher;

import horme.util.config;
import horme.thrift.AgentService;
import horme.thrift.agentservice_types;
import horme.util.array;
import horme.util.log;
import thrift.base;
import thrift.transport.buffered;
import thrift.codegen.client;
import thrift.transport.socket;
import thrift.protocol.binary;
import std.array : popFront;

class Pusher {

private:
    AgentConfig _config;
    MetricData[] _metrics;
    string[] _invalidControllers; 
    TBufferedTransport _currentAgentTransport;
    AgentService _currentAgentService;
    bool _disabled = false;
    //TODO: make this configurable
    int _maxMetricBuffer = 1000;

protected:

    void resetControllers() {
        if (_currentAgentTransport !is null && _currentAgentTransport.isOpen) {
            try {
                _currentAgentTransport.close();
            } catch (Exception e) {
                //TODO: log
            }
        }
        _currentAgentTransport = null;
        _currentAgentService = null;
        _invalidControllers.clear();
    }

    void openController(ControllerConfig controller) {
        _currentAgentTransport = null;
        _currentAgentService = null;
        if (!_invalidControllers.contains(controller.id)) {
            try {
                //connect
                auto socket = new TSocket(cast(string) controller.host, 
                    cast(ushort) controller.port);
                auto transport = new TBufferedTransport(socket);
                auto protocol = tBinaryProtocol(transport);
                auto client = tClient!AgentService(protocol);
                transport.open();
                info("Connected to controller: %s", controller);
                //success, assign
                _currentAgentTransport = transport;
                _currentAgentService = client;
            } catch (Exception e) {
                warn("Controller %s failed, marking invalid", controller, e);
                _invalidControllers ~= controller.host;
            }
        }
    }

    AgentService getCurrentAgentService() {
        if (_disabled) {
            return null;
        }
        //open?
        if (_currentAgentTransport is null || !_currentAgentTransport.isOpen) {
            //if the primary is available, use it
            openController(_config.controllers[_config.primaryControllerId]);
            if (_currentAgentTransport is null || !_currentAgentTransport.isOpen) {
                //try all the others
                foreach (controller; _config.controllers) {
                    openController(_config.controllers[_config.primaryControllerId]);
                    if (_currentAgentTransport !is null && _currentAgentTransport.isOpen) {
                        return _currentAgentService;
                    }
                }
                warn("No available controllers, disabling agent");
                //TODO: log
                _disabled = true;
            }
        }
        return _currentAgentService;
    }

    bool handleUpdate(AgentUpdate update) {
        if (update.isSet!"config") {
            //set the config
            _config = update.config;
            //reset the controllers
            resetControllers();
            return true;
        }
        return false;
    }

public:
    this(AgentConfig config) {
        _config = config;
    }

    ~this() {
        resetControllers();
    }

    @property
    const(AgentConfig) config() {
        return _config;
    }

    @property
    bool disabled() {
        return _disabled;
    }

    void addMetric(MetricData metric) {
        if (!_disabled) {
            //if the max buffer is over size, pop before adding
            if (_metrics.length >= _maxMetricBuffer) {
                _metrics.popFront();
            }
            _metrics ~= metric;
        }
    }

    ///Returns: true if the config changed
    bool push() {
        if (_disabled) {
            return false;
        }
        auto service = getCurrentAgentService();
        //no service?
        if (service is null) {
            trace("No controller %s", service);
            //no go
            return false;
        }
        AgentUpdate update;
        //no metrics?
        if (_metrics.empty) {
            //just ping then
            trace("No metrics, just pinging");
            update = service.ping(_config.id);
        } else {
            //send metrics
            trace("Posting %s metrics", _metrics.length);
            update = service.postMetrics(_config.id, _metrics);
            //clear metrics
            _metrics.clear();
        }
        //handle update
        return handleUpdate(update);
    }
}