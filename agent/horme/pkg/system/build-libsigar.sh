#!/bin/bash
#
# build-libsigar.sh
#   Build script for sigar library
#

# Backup distributed CMake file
cp -fp sigar/src/CMakeLists.txt{,.dist}

# Patch CMake file
patch -p0 < libsigar-static.patch

# Navigate to source directory
cd sigar

# Create build directory
mkdir build
pushd build

# Build sigar library
cmake ..
make

# Copy sigar library
cp build-src/libsigar.a ../../

# Clean up 
popd
rm -rf build
cd ../

# Restore distributed CMake file
cp -fp sigar/src/CMakeLists.txt{.dist,}
rm -f sigar/src/CMakeLists.txt.dist
