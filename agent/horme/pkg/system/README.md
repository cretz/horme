# Windows

## Build MSVC Project

* Install gyp
* Run: gyp build-sigar.gyp 
** Note, you may have to use --depth to set the path to the sigar root

## Build Windows DLL
* Install Visual C++ Express 2008
* Run: "c:\Program Files\Microsoft Visual Studio 9.0\VC\vcpackages\vcbuild" sigar.vcproj
* The DLL will be at Default\sigar.dll

## Get Lib Ready for DMD Inclusion
* Import the lib using "implib sigar.lib sigar.dll" (implib can be found here: http://ftp.digitalmars.com/bup.zip)
* Put lib in this folder (i.e. system)

# Linux
* Run: ./build-libsigar.sh
