module horme.pkg.system.lib;

extern (C):

//aliases
alias int sigar_int32_t;
alias long sigar_int64_t;
alias uint sigar_uint32_t;
alias ulong sigar_uint64_t;

//consts
const SIGAR_OK = 0;

//structs, in alphabetical order sans trailing _t

struct sigar_t { }
struct sigar_cpu_t {
    sigar_uint64_t user;
    sigar_uint64_t sys;
    sigar_uint64_t nice;
    sigar_uint64_t idle;
    sigar_uint64_t wait;
    sigar_uint64_t irq;
    sigar_uint64_t soft_irq;
    sigar_uint64_t stolen;
    sigar_uint64_t total;
}
struct sigar_cpu_list_t {
    uint number;
    uint size;
    sigar_cpu_t* data;
}
struct sigar_loadavg_t {
    double[3] loadavg;
}
struct sigar_mem_t {
    sigar_uint64_t ram;
    sigar_uint64_t total;
    sigar_uint64_t used;
    sigar_uint64_t free;
    sigar_uint64_t actual_used;
    sigar_uint64_t actual_free;
    double used_percent;
    double free_percent;
}
struct sigar_proc_list_t {
    uint number;
    uint size;
    sigar_uint64_t *data;
}
struct sigar_proc_stat_t
{
    sigar_uint64_t total;
    sigar_uint64_t sleeping;
    sigar_uint64_t running;
    sigar_uint64_t zombie;
    sigar_uint64_t stopped;
    sigar_uint64_t idle;
    sigar_uint64_t threads;
}
struct sigar_swap_t {
    sigar_uint64_t total;
    sigar_uint64_t used;
    sigar_uint64_t free;
    sigar_uint64_t page_in;
    sigar_uint64_t page_out;
}
struct sigar_uptime_t
{
    double uptime;
}

// external functions, in alphabetical order
extern (System):
int sigar_close(sigar_t *sigar);
int sigar_cpu_list_get(sigar_t *sigar, sigar_cpu_list_t *cpulist);
int sigar_cpu_list_destroy(sigar_t *sigar, sigar_cpu_list_t *cpulist);
int sigar_loadavg_get(sigar_t *sigar, sigar_loadavg_t *loadavg);
int sigar_mem_get(sigar_t *sigar, sigar_mem_t *mem);
int sigar_open(sigar_t **sigar);
int sigar_proc_list_get(sigar_t *sigar, sigar_proc_list_t *proclist);
int sigar_proc_list_destroy(sigar_t *sigar, sigar_proc_list_t *proclist);
int sigar_proc_stat_get(sigar_t *sigar, sigar_proc_stat_t *procstat);
char* sigar_strerror(sigar_t *sigar, int err);
int sigar_swap_get(sigar_t *sigar, sigar_swap_t *swap);
int sigar_uptime_get(sigar_t *sigar, sigar_uptime_t *uptime);
