{
  'targets': [
    {
      'target_name': 'sigar',
      'type': 'shared_library',
      'sources': [
        'sigar/src/sigar.c',
        'sigar/src/sigar_cache.c',
        'sigar/src/sigar_fileinfo.c',
        'sigar/src/sigar_format.c',
        'sigar/src/sigar_getline.c',
        'sigar/src/sigar_ptql.c',
        'sigar/src/sigar_signal.c',
        'sigar/src/sigar_util.c'
      ],
      'include_dirs': [
        'sigar/include'
      ],
      'conditions': [
        ['OS=="win"',
          {
            'defines': [
              'WIN32'
            ],
            'sources': [
              'sigar/src/os/win32/peb.c',
              'sigar/src/os/win32/win32_sigar.c',
              'sigar/src/os/win32/wmi.cpp'
            ],
            'include_dirs': [
              'sigar/src/os/win32'
            ],
            'libraries': [
              'ws2_32.lib',
              'netapi32.lib',
              'version.lib'
            ]
          }
        ]
      ]
    }
  ]
}