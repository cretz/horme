module horme.pkg.system.pkg;

import horme.thrift.agentservice_types;
import horme.pkg.metricpackage;
import horme.pkg.system.lib;
import horme.util.string;
import horme.util.log;
import std.format : formattedWrite;
import std.array : appender;
import std.conv : to;
import std.string : isNumeric;

class SystemPackage : MetricPackage {

    shared static const string[] METRIC_NAMES;
    
    shared static this() {
        //please keep in alphabetical order
        auto a = appender([
            "horme.system.cpu.idle",
            "horme.system.cpu.irq",
            "horme.system.cpu.nice",
            "horme.system.cpu.softirq",
            "horme.system.cpu.stolen",
            "horme.system.cpu.sys",
            "horme.system.cpu.total",
            "horme.system.cpu.user",
            "horme.system.cpu.wait",
            "horme.system.mem.actualfree",
            "horme.system.mem.actualused",
            "horme.system.mem.free",
            "horme.system.mem.freepct",
            "horme.system.mem.ram",
            "horme.system.mem.total",
            "horme.system.mem.used",
            "horme.system.mem.usedpct",
            "horme.system.procstat.idle",
            "horme.system.procstat.running",
            "horme.system.procstat.sleeping",
            "horme.system.procstat.stopped",
            "horme.system.procstat.threads",
            "horme.system.procstat.total",
            "horme.system.procstat.zombie",
            "horme.system.swap.free",
            "horme.system.swap.pagein",
            "horme.system.swap.pageout",
            "horme.system.swap.total",
            "horme.system.swap.used",
            "horme.system.uptime",
        ]);
        version (Posix) {
            a.put([
                "horme.system.load.1",
                "horme.system.load.5",
                "horme.system.load.15",
            ]);
        }
        METRIC_NAMES = cast(shared) cast(const) a.data;
    }

    @property
    const(string[]) metrics() {
        return cast(string[]) METRIC_NAMES;
    }

    MetricPoller createPoller() {
        return new SystemPoller();
    }
}

class SystemPoller : MetricPoller {

private:
    sigar_t* sigar_;

public:
    this() {
        trace("Opening SIGAR");
        checkSigar(sigar_open(&sigar_), "Unable to open sigar package");
    }

    ~this() {
        trace("Closing SIGAR");
        sigar_close(sigar_);
    }

    MetricData poll(string metricName, string[string] params) {
        MetricData data;
        synchronized (this) {
            switch (metricName) {
                //please keep in alphabetical order
                case "horme.system.cpu.idle":
                    pollCpu!"idle"(&data, params);
                    break;
                case "horme.system.cpu.irq":
                    pollCpu!"irq"(&data, params);
                    break;
                case "horme.system.cpu.nice":
                    pollCpu!"nice"(&data, params);
                    break;
                case "horme.system.cpu.softirq":
                    pollCpu!"soft_irq"(&data, params);
                    break;
                case "horme.system.cpu.stolen":
                    pollCpu!"stolen"(&data, params);
                    break;
                case "horme.system.cpu.sys":
                    pollCpu!"sys"(&data, params);
                    break;
                case "horme.system.cpu.total":
                    pollCpu!"total"(&data, params);
                    break;
                case "horme.system.cpu.user":
                    pollCpu!"user"(&data, params);
                    break;
                case "horme.system.cpu.wait":
                    pollCpu!"wait"(&data, params);
                    break;
                case "horme.system.load.1":
                    pollLoad(&data, 0);
                    break;
                case "horme.system.load.5":
                    pollLoad(&data, 1);
                    break;
                case "horme.system.load.15":
                    pollLoad(&data, 2);
                    break;
                case "horme.system.mem.actualfree":
                    pollMem!("actual_free", string)(&data);
                    break;
                case "horme.system.mem.actualused":
                    pollMem!("actual_used", string)(&data);
                    break;
                case "horme.system.mem.free":
                    pollMem!("free", string)(&data);
                    break;
                case "horme.system.mem.freepct":
                    pollMem!("free_percent", double)(&data);
                    break;
                case "horme.system.mem.ram":
                    pollMem!("ram", string)(&data);
                    break;
                case "horme.system.mem.total":
                    pollMem!("total", string)(&data);
                    break;
                case "horme.system.mem.used":
                    pollMem!("used", string)(&data);
                    break;
                case "horme.system.mem.usedpct":
                    pollMem!("used_percent", double)(&data);
                    break;
                case "horme.system.procstat.idle":
                    pollProcStat!"idle"(&data);
                    break;
                case "horme.system.procstat.running":
                    pollProcStat!"running"(&data);
                    break;
                case "horme.system.procstat.sleeping":
                    pollProcStat!"sleeping"(&data);
                    break;
                case "horme.system.procstat.stopped":
                    pollProcStat!"stopped"(&data);
                    break;
                case "horme.system.procstat.threads":
                    pollProcStat!"threads"(&data);
                    break;
                case "horme.system.procstat.total":
                    pollProcStat!"total"(&data);
                    break;
                case "horme.system.procstat.zombie":
                    pollProcStat!"zombie"(&data);
                    break;
                case "horme.system.swap.free":
                    pollSwap!"free"(&data);
                    break;
                case "horme.system.swap.pagein":
                    pollSwap!"page_in"(&data);
                    break;
                case "horme.system.swap.pageout":
                    pollSwap!"page_out"(&data);
                    break;
                case "horme.system.swap.total":
                    pollSwap!"total"(&data);
                    break;
                case "horme.system.swap.used":
                    pollSwap!"used"(&data);
                    break;
                case "horme.system.uptime":
                    sigarGet!("sigar_uptime_t", "sigar_uptime_get", "uptime", double)(&data);
                    break;
                default:
                    data.message = "Unrecognized metric name " ~ metricName;
            }
        }
        return data;
    }

private:

    void checkSigar(int status, string prefix) {
        if (status != SIGAR_OK) {
            auto msg = appender!string();
            string err = fromStringz(sigar_strerror(sigar_, status));
            formattedWrite(msg, "%s: %s (%s)", prefix, status, err);
            throw new MetricException(msg.data);
        }
    }

    void sigarGet(string sigarStructName, string sigarFuncName, 
            string member, T)(MetricData* data) {
        mixin(sigarStructName ~ " item;");
        checkSigar(mixin(sigarFuncName)(sigar_, &item), 
            "Unable to call " ~ sigarFuncName);
        static if (is(T == double)) {
            data.set!"dataDouble"(__traits(getMember, item, member));
        } else static if (is(T == string)) {
            data.set!"dataString"(to!string(__traits(getMember, item, member)));
        } else {
            static assert(0, "Unknown type " ~ T.stringof);
        }
    }

    void pollCpu(string member)(MetricData* data, string[string] params) {
        //number present?
        if ("number" !in params) {
            data.set!"message"("Number is required");
            return;
        }
        auto numberString = params["number"];
        //int?
        if (!isNumeric(numberString)) {
            data.set!"message"("Invalid number format: " ~ numberString);
        }
        auto number = to!int(numberString);
        if (number < 0) {
            data.set!"message"("Number " ~ numberString ~ " cannot be less than zero");
            return;
        }
        sigar_cpu_list_t cpulist;
        checkSigar(sigar_cpu_list_get(sigar_, &cpulist), "Unable to get CPUs");
        scope(exit) sigar_cpu_list_destroy(sigar_, &cpulist);
        //number there?
        if (number >= cpulist.number) {
            data.set!"message"("Number " ~ numberString ~ 
                " is higher index than total CPUs: " ~ to!string(cpulist.number));
            return;
        }
        auto cpu = cpulist.data[number];
        data.set!"dataString"(to!string(__traits(getMember, cpu, member)));
    }

    void pollLoad(MetricData* data, int which) {
        version (Windows) {
            throw new MetricException("Load not available on Windows");
        } else {
            sigar_loadavg_t loadavg;
            checkSigar(sigar_loadavg_get(sigar_, &loadavg), "Unable to get load");
            data.dataDouble = loadavg.loadavg[which];
        }
    }

    void pollMem(string member, T)(MetricData* data) {
        sigarGet!("sigar_mem_t", "sigar_mem_get", member, T)(data);
    }

    void pollProcStat(string member)(MetricData* data) {
        sigarGet!("sigar_proc_stat_t", "sigar_proc_stat_get", member, string)(data);
    }

    void pollSwap(string member)(MetricData* data) {
        sigarGet!("sigar_swap_t", "sigar_swap_get", member, string)(data);
    }

    unittest {
        import std.stdio;
        auto val = new SystemPoller;
        void valCheck(string str)(string metric, string[string] params = null) {
            auto data = val.poll(metric, params);
            assert(data.isSet!str, "Unable to get " ~ metric);
            trace("%s: %s", metric, __traits(getMember, data, str));
        }
        //keep in alphabetical order please
        auto params = ["number": "1"];
        valCheck!"dataString"("horme.system.cpu.idle", params);
        valCheck!"dataString"("horme.system.cpu.irq", params);
        valCheck!"dataString"("horme.system.cpu.nice", params);
        valCheck!"dataString"("horme.system.cpu.softirq", params);
        valCheck!"dataString"("horme.system.cpu.stolen", params);
        valCheck!"dataString"("horme.system.cpu.sys", params);
        valCheck!"dataString"("horme.system.cpu.total", params);
        valCheck!"dataString"("horme.system.cpu.user", params);
        valCheck!"dataString"("horme.system.cpu.wait", params);
        valCheck!"dataString"("horme.system.mem.actualused");
        valCheck!"dataString"("horme.system.mem.actualfree");
        valCheck!"dataString"("horme.system.mem.free");
        valCheck!"dataDouble"("horme.system.mem.freepct");
        valCheck!"dataString"("horme.system.mem.ram");
        valCheck!"dataString"("horme.system.mem.total");
        valCheck!"dataString"("horme.system.mem.used");
        valCheck!"dataDouble"("horme.system.mem.usedpct");
        valCheck!"dataString"("horme.system.procstat.idle");
        valCheck!"dataString"("horme.system.procstat.running");
        valCheck!"dataString"("horme.system.procstat.sleeping");
        valCheck!"dataString"("horme.system.procstat.stopped");
        valCheck!"dataString"("horme.system.procstat.threads");
        valCheck!"dataString"("horme.system.procstat.total");
        valCheck!"dataString"("horme.system.procstat.zombie");
        valCheck!"dataString"("horme.system.swap.free");
        valCheck!"dataString"("horme.system.swap.pagein");
        valCheck!"dataString"("horme.system.swap.pageout");
        valCheck!"dataString"("horme.system.swap.total");
        valCheck!"dataString"("horme.system.swap.used");
        valCheck!"dataDouble"("horme.system.uptime");
    }
}