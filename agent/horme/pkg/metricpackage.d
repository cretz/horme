module horme.pkg.metricpackage;

import horme.thrift.agentservice_types;
import horme.exception;
import horme.util.log;

class MetricPackages {
private:
    static void initPackage(T : MetricPackage)(string packageName) {
        auto pkg = new T();
        foreach (metric; pkg.metrics) {
            packagesByMetric[metric] = packageName;
            packages[packageName] = pkg;
        }
    }

    static this() {
        //put your packages here... (please keep in alphabetical order)
        initPackage!(horme.pkg.system.pkg.SystemPackage)("horme.system");
    }
public:
    static string[string] packagesByMetric;
    ///these are not thread safe, expected to have caller to be smart
    static MetricPackage[string] packages;
}

interface MetricPackage {
    @property
    nothrow const(string[]) metrics();

    MetricPoller createPoller();
}

interface MetricPoller {

    MetricData poll(string metricName, string[string] params);

}

class MetricException : HormeException {
    this(string msg = "", string file = __FILE__, size_t line = __LINE__,
            Throwable next = null) {
        super(msg, file, line, next);
    }
}