/*
 * This auto-generated skeleton file illustrates how to build a server. If you
 * intend to customize it, you should edit a copy with another file name to 
 * avoid overwriting it when running the generator again.
 */
module horme.thrift.AgentService_server;

import std.stdio;
import thrift.codegen.processor;
import thrift.protocol.binary;
import thrift.server.simple;
import thrift.server.transport.socket;
import thrift.transport.buffered;
import thrift.util.hashset;

import horme.thrift.AgentService;
import horme.thrift.agentservice_types;


class AgentServiceHandler : AgentService {
  this() {
    // Your initialization goes here.
  }

  AgentConfig getConfig(string agentId) {
    // Your implementation goes here.
    writeln("getConfig called");
    return typeof(return).init;
  }

  AgentUpdate ping(string agentId) {
    // Your implementation goes here.
    writeln("ping called");
    return typeof(return).init;
  }

  AgentUpdate postMetrics(string agentId, MetricData[] metrics) {
    // Your implementation goes here.
    writeln("postMetrics called");
    return typeof(return).init;
  }

}

void main() {
  auto protocolFactory = new TBinaryProtocolFactory!();
  auto processor = new TServiceProcessor!AgentService(new AgentServiceHandler);
  auto serverTransport = new TServerSocket(9090);
  auto transportFactory = new TBufferedTransportFactory;
  auto server = new TSimpleServer(
    processor, serverTransport, transportFactory, protocolFactory);
  server.serve();
}
