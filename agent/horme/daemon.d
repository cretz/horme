module horme.daemon;

import horme.thrift.agentservice_types;
import horme.util.config;
import horme.util.log;
import horme.pusher;
import horme.pkg.metricpackage;
import std.concurrency : Tid, spawn, receiveTimeout, receive, thisTid;
import std.datetime : Clock, stdTimeToUnixTime;
import std.variant : Variant;
import core.time : convert, dur;

class Daemon {
private:
    string _configFilename;
    AgentConfig _config;
    Pusher _pusher;
    Tid[] _collectors;
    int _nextCollectorAssignmentIndex = 0;
    int[string] _packageCollectorIndexes;
    int _threadPoolSize;

    bool pushAndCheckConfig() {
        if (_pusher.push()) {
            //grab config
            _config = cast(AgentConfig) _pusher.config;
            //save it
            trace("Agent update received, saving");
            _config.saveConfigToFile(_configFilename);
            //update which packages go to which collectors
            updateMetricPackagesOnCollectors();
            return true;
        }
        return false;
    }

    void updateMetricPackagesOnCollectors() {
        trace("Updating packages on controllers");
        //loop
        MetricConfig[][int] configsByCollector;
        foreach (metric; _config.metrics) {
            //get package name
            if (metric.name !in MetricPackages.packagesByMetric) {
                warn("Metric %s not available on system, ignoring", metric.name);
                continue;
            }
            string packageName = MetricPackages.packagesByMetric[metric.name];
            int collectorIndex;
            if (packageName !in _packageCollectorIndexes) {
                //assign
                _packageCollectorIndexes[packageName] = _nextCollectorAssignmentIndex;
                collectorIndex = _nextCollectorAssignmentIndex;
                //increment
                ++_nextCollectorAssignmentIndex;
                if (_nextCollectorAssignmentIndex == _threadPoolSize) {
                    _nextCollectorAssignmentIndex = 0;
                }
            } else {
                collectorIndex = _packageCollectorIndexes[packageName];
            }
            //add the metric to its proper index
            configsByCollector[collectorIndex] ~= metric;
        }
        //inform collectors
        foreach (index, collector; _collectors) {
            if (index !in configsByCollector) {
                //must be empty then
                collector.send(UpdateCollectorMetrics());
            } else {
                collector.send(UpdateCollectorMetrics(
                    cast(shared)configsByCollector[index]));
            }
        }
    }

public:
    this(string configFilename, int threadPoolSize) {
        _configFilename = configFilename;
        _threadPoolSize = threadPoolSize;
    }

    void start() {
        //load config
        info("Loading config from %s", _configFilename);
        _config = loadConfigFromFile(_configFilename);
        //load pusher
        trace("Creating pusher");
        _pusher = new Pusher(_config);
        //create all threads now
        foreach (i; 0.._threadPoolSize) {
            auto tid = spawn(&collector, thisTid);
            trace("Spawned: %s", tid);
            _collectors ~= tid;
        }
        //quick ping
        info("Pinging controller");
        if (!pushAndCheckConfig()) {
            //it didn't get an update? ok, we'll check packages
            updateMetricPackagesOnCollectors();
        }
        //main program loop
        long sinceLastPush = Clock.currStdTime;
        while (!_pusher.disabled) {
            //receive all messages
            bool received;
            long timeout;
            do {
                //get the timeout as how many ms is remaining until push
                timeout = _config.heartbeat - 
                    convert!("hnsecs", "msecs")(Clock.currStdTime - sinceLastPush);
                trace("Waiting %s ms before push", timeout);
                received = false;
                //negative or zero means we need a push
                if (timeout > 0) {
                    //receive or timeout since last push
                    received = receiveTimeout(dur!"msecs"(timeout), 
                        (SharedMetricData data) {
                            _pusher.addMetric(data.fromShared());
                        });
                }
            } while (received);
            //we know we need a push, because the above doesn't exit until we do
            pushAndCheckConfig();
            //we update the time AFTER the push
            sinceLastPush = Clock.currStdTime;
        }
    }
}

shared struct UpdateCollectorMetrics {
    MetricConfig[] metrics;
}

void collector(Tid owner) {
    MetricConfig[] metrics;
    //time since last poll in hnsecs
    long[string] timesOfLastMetrics;
    MetricPoller[string] pollersByPackage;
    do {
        //to update metrics
        void updateMetrics(UpdateCollectorMetrics metricUpdate) {
            trace("Received collector update: %s", metricUpdate);
            metrics.clear();
            bool[string] packagesSeen;
            foreach (metric; metricUpdate.metrics) {
                if (metric.name in MetricPackages.packagesByMetric) {
                    metrics ~= cast(MetricConfig)metric;
                    //lazy package check
                    auto packageName = MetricPackages.packagesByMetric[metric.name];
                    packagesSeen[packageName] = true;
                    //need to create the poller?
                    if (packageName !in pollersByPackage) {
                        trace("Creating poller for %s", packageName);
                        //create it
                        pollersByPackage[packageName] = MetricPackages.
                            packages[packageName].createPoller();
                    }
                }
            }
            //loop through pollers, removing ones that don't have metrics any more
            string[] pollerPackagesToRemove;
            foreach (packageName, poller; pollersByPackage) {
                if (packageName !in packagesSeen) {
                    //make note to remove because I don't want to alter inside of iteration
                    pollerPackagesToRemove ~= packageName;
                }
            }
            foreach (pollerPackageToRemove; pollerPackagesToRemove) {
                pollersByPackage.remove(pollerPackageToRemove);
            }
            //loop through the metrics and remove times of metrics no longer present
            long[string] newTimesOfLastMetrics;
            foreach (metric; metrics) {
                if (metric.id in timesOfLastMetrics) {
                    newTimesOfLastMetrics[metric.id] = timesOfLastMetrics[metric.id];
                }
            }
            timesOfLastMetrics = newTimesOfLastMetrics;
        }
        //as long as we don't have any metrics, wait until we get them
        trace("Waiting to receive metrics");
        while (metrics.length == 0) {
            receive(&updateMetrics);
        }
        //loop through all the metrics, checking if they need to be polled for
        //the shortest amount of ms of all metrics before we need to poll again
        long shortestTimeForNext = 60000; //default to 1 minuter
        foreach (metric; metrics) {
            //need a poll for this metric?
            bool needsPoll = metric.id !in timesOfLastMetrics;
            long remaining;
            if (!needsPoll) {
                remaining = metric.executionFrequency - 
                    convert!("hnsecs", "msecs")(Clock.currStdTime - timesOfLastMetrics[metric.id]);
                trace("Poll not needed for metric %s, remaining %s ms", metric.id, remaining);
                needsPoll = remaining <= 0;
            }
            if (needsPoll) {
                //poll
                auto poller = pollersByPackage[MetricPackages.packagesByMetric[metric.name]];
                auto data = poller.poll(metric.name, metric.params);
                auto metricTime = Clock.currStdTime;
                data.id = metric.id;
                data.time = stdTimeToUnixTime(metricTime);
                trace("Received metric: %s", data.id);
                //update time since last
                timesOfLastMetrics[metric.id] = metricTime;
                //send data
                owner.send(data.toShared());
                //remaining is now full frequency
                remaining = metric.executionFrequency;
            }
            //this has fewest remaining?
            if (remaining < shortestTimeForNext) {
                shortestTimeForNext = remaining;
            }
        }
        //now, let's just sit and wait for metrics until our next time to poll 
        //  OR we receive metric updates
        receiveTimeout(dur!"msecs"(shortestTimeForNext), &updateMetrics);
    } while (true);
}