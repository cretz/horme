module horme.util.config;

import std.stdio : writeln;
import std.conv : to;
import std.file : readText, write;
import std.json : parseJSON, toJSON;
import horme.util.json;
import std.exception : enforce;
import horme.thrift.AgentService;
import horme.thrift.agentservice_types;


AgentConfig loadConfigFromFile(string filename) {
    //TODO better exception handling
    return loadConfigFromString(readText(filename));
}

AgentConfig loadConfigFromString(string text) {
    //TODO better exception handling
    AgentConfig config;
    JSONValue json = parseJSON(text);
    //get ID
    config.id = json.enforceJson!string("id");
    //get heartbeat
    config.heartbeat = json.enforceJson!long("heartbeat");
    //get primary controller ID
    config.primaryControllerId = json.enforceJson!string("primaryControllerId");
    //get controllers
    auto controllersJson = json.enforceJson!JSONArray("controllers");
    foreach (controllerJson; controllersJson) {
        ControllerConfig controller = ControllerConfig();
        controller.id = controllerJson.enforceJson!string("id");
        controller.host = controllerJson.enforceJson!string("host");
        controller.port = controllerJson.enforceJson!int("port");
        config.controllers[controller.id] = controller;
    }
    //get metrics
    auto metricsJson = json.enforceJson!JSONArray("metrics");
    foreach (metricJson; metricsJson) {
        MetricConfig metric = MetricConfig();
        metric.id = metricJson.enforceJson!string("id");
        metric.name = metricJson.enforceJson!string("name");
        metric.executionFrequency = metricJson.enforceJson!long("executionFrequency");
        metric.haltOnFailure = metricJson.enforceJson!bool("haltOnFailure");
        //params
        if ("params" in metricJson.object) {
            auto paramsJson = metricJson.enforceJson!JSONObject("params");
            foreach (key, value; paramsJson) {
                metric.params[key] = value.str;
            }
        }
        config.metrics[metric.id] = metric;
    }
    return config;
}

unittest {
    AgentConfig config = loadConfigFromString(`{
"id": "1234",
"heartbeat": "5678",
"primaryControllerId": "111",
"controllers": [
    {"id": "111", "host": "1.2.3.4", "port": "101"},
    {"id": "222", host": "5.6.7.8", "port": "202"}
],
"metrics": [
    {
        "id": "9012",
        "name": "my.metric.name",
        "executionFrequency": 3456,
        "haltOnFailure": true,
        "params": {
            "key1": "val1",
            "key2": "val2"
        }
    },
    {
        "id": "7890",
        "name": "my.metric.name2",
        "executionFrequency": 1234,
        "haltOnFailure": false
    }
]
}`);
    assert(config.id == "1234");
    assert(config.heartbeat == 5678);
    assert(config.primaryControllerId == "111");
    assert(config.controllers.length == 2);
    assert(config.controllers["111"] ==  ControllerConfig("111", "1.2.3.4", 101));
    assert(config.controllers["222"] == ControllerConfig("222", "5.6.7.8", 202));
    assert(config.metrics.length == 2);
    auto metric = config.metrics["9012"];
    assert(metric.id == "9012");
    assert(metric.name == "my.metric.name");
    assert(metric.executionFrequency == 3456);
    assert(metric.haltOnFailure);
    assert(metric.params.length == 2);
    assert(metric.params["key1"] == "val1");
    assert(metric.params["key2"] == "val2");
    auto metric2 = config.metrics["7890"];
    assert(metric2 == MetricConfig("7890", "my.metric.name2", 1234, false));
    assert(metric2.params.length == 0);
    //writeln("JSON: ", config.saveConfigToString());
}

void saveConfigToFile(AgentConfig config, string filename) {
    write(filename, saveConfigToString(config));
}

string saveConfigToString(AgentConfig config) {
    JSONObject json;
    //vals
    json["id"] = jsonValue(config.id);
    json["heartbeat"] = jsonValue(config.heartbeat);
    json["primaryControllerId"] = jsonValue(config.primaryControllerId);
    //controllers
    JSONArray controllersJson;
    foreach (controller; config.controllers) {
        JSONObject controllerJson;
        controllerJson["id"] = jsonValue(controller.id);
        controllerJson["host"] = jsonValue(controller.host);
        controllerJson["port"] = jsonValue(controller.port);
        controllersJson ~= jsonValue(controllerJson);
    }
    json["controllers"] = jsonValue(controllersJson);
    //metrics
    JSONArray metricsJson;
    foreach (metric; config.metrics) {
        JSONObject metricJson;
        metricJson["id"] = jsonValue(metric.id);
        metricJson["name"] = jsonValue(metric.name);
        metricJson["executionFrequency"] = jsonValue(metric.executionFrequency);
        metricJson["haltOnFailure"] = jsonValue(metric.haltOnFailure);
        if (metric.params.length > 0) {
            JSONObject paramsJson;
            foreach (key, value; metric.params) {
                paramsJson[key] = jsonValue(value);
            }
            metricJson["params"] = jsonValue(paramsJson);
        }
        metricsJson ~= jsonValue(metricJson);
    }
    json["metrics"] = jsonValue(metricsJson);
    //to string
    auto jsonVal = jsonValue(json);
    return toJSON(&jsonVal);
}

///This is only here because thrift generated structs aren't sharable easily
shared struct SharedMetricData {
  string id;

  long time;

  bool dataBool;
  bool isDataBoolSet;

  byte dataByte;
  bool isDataByteSet;

  short dataI16;
  bool isDataI16Set;

  int dataI32;
  bool isDataI32Set;

  long dataI64;
  bool isDataI64Set;

  double dataDouble;
  bool isDataDoubleSet;

  string dataString;
  bool isDataStringSet;

  string[] dataList;
  bool isDataListSet;

  string[string] dataMap;
  bool isDataMapSet;
  
  string message;
  bool isMessageSet;
}
//TODO: this is stupid...
SharedMetricData toShared(MetricData data) {
    auto ret = SharedMetricData();
    ret.id = data.id;
    ret.time = data.time;

    ret.isDataBoolSet = data.isSet!"dataBool";
    if (ret.isDataBoolSet) ret.dataBool = data.dataBool;

    ret.isDataByteSet = data.isSet!"dataByte";
    if (ret.isDataByteSet) ret.dataByte = data.dataByte;

    ret.isDataI16Set = data.isSet!"dataI16";
    if (ret.isDataI16Set) ret.dataI16 = data.dataI16;

    ret.isDataI32Set = data.isSet!"dataI32";
    if (ret.isDataI32Set) ret.dataI32 = data.dataI32;

    ret.isDataI64Set = data.isSet!"dataI64";
    if (ret.isDataI64Set) ret.dataI64 = data.dataI64;

    ret.isDataDoubleSet = data.isSet!"dataDouble";
    if (ret.isDataDoubleSet) ret.dataDouble = data.dataDouble;

    ret.isDataStringSet = data.isSet!"dataString";
    if (ret.isDataStringSet) ret.dataString = data.dataString;

    ret.isDataListSet = data.isSet!"dataList";
    if (ret.isDataListSet) ret.dataList = cast(shared(string[]))data.dataList;

    ret.isDataMapSet = data.isSet!"dataMap";
    if (ret.isDataMapSet) ret.dataMap = cast(shared(string[string]))data.dataMap;

    ret.isMessageSet = data.isSet!"message";
    if (ret.isMessageSet) ret.message = data.message;

    return ret;
}

MetricData fromShared(SharedMetricData data) {
    auto ret = MetricData();
    ret.id = data.id;
    ret.time = data.time;

    if (data.isDataBoolSet) ret.set!"dataBool"(data.dataBool);
    if (data.isDataByteSet) ret.set!"dataByte"(data.dataByte);
    if (data.isDataI16Set) ret.set!"dataI16"(data.dataI16);
    if (data.isDataI32Set) ret.set!"dataI32"(data.dataI32);
    if (data.isDataI64Set) ret.set!"dataI64"(data.dataI64);
    if (data.isDataDoubleSet) ret.set!"dataDouble"(data.dataDouble);
    if (data.isDataStringSet) ret.set!"dataString"(data.dataString);
    if (data.isDataListSet) ret.set!"dataList"(cast(string[])data.dataList);
    if (data.isDataMapSet) ret.set!"dataMap"(cast(string[string])data.dataMap);
    if (data.isMessageSet) ret.set!"message"(data.message);

    return ret;
}