module horme.util.json;

import std.json : JSONValue, JSON_TYPE;
import std.conv : isSomeString, isBoolean, isAssignable, to;
import std.exception : enforce;

alias JSONValue[string] JSONObject;
alias JSONValue[] JSONArray;

T enforceJson(T)(JSONValue json, string value) {
    auto object = json.object;
    enforce(json.object !is null, "Unable to get " ~ value ~ " from non-object");
    enforce(value in object, value ~ " required");
    auto val = object[value];
    T ret;
    static if (isSomeString!T) {
        ret = val.str;
    } else static if (isBoolean!T) {
        enforce(val.type == JSON_TYPE.TRUE || val.type == JSON_TYPE.FALSE,
            value ~ " must be a boolean");
        ret = val.type == JSON_TYPE.TRUE;
    } else static if (isAssignable!(long, T)) {
        ret = to!T(val.integer);
    } else static if (isAssignable!(real, T)) {
        ret = val.floating;
    } else static if (isAssignable!(JSONArray, T)) {
        ret = val.array;
    } else static if (isAssignable!(JSONObject, T)) {
        ret = val.object;
    } else {
        assert(0, "Unknown type: " ~ value);
    }
    enforce(isBoolean!T || ret !is T.init, value ~ " is not present or unexpected type");
    return ret;
}

JSONValue jsonValue(T)(T value) {
    JSONValue ret = JSONValue();
    static if (isSomeString!T) {
        ret.str = value;
        ret.type = JSON_TYPE.STRING;
    } else static if (isBoolean!T) {
        ret.type = value ? JSON_TYPE.TRUE : JSON_TYPE.FALSE;
    } else static if (isAssignable!(long, T)) {
        ret.integer = value;
        ret.type = JSON_TYPE.INTEGER;
    } else static if (isAssignable!(real, T)) {
        ret.floating = value;
        ret.type = JSON_TYPE.FLOAT;
    } else static if (isAssignable!(JSONArray, T)) {
        ret.array = value;
        ret.type = JSON_TYPE.ARRAY;
    } else static if (isAssignable!(JSONObject, T)) {
        ret.object = value;
        ret.type = JSON_TYPE.OBJECT;
    } else {
        assert(0, "Unknown type: " ~ T);
    }
    return ret;
}