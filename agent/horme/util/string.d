module horme.util.string;

import std.conv : to;

string fromStringz(char* cstring) {
    return to!string(cstring);
}

string fromStringz(char[] cstring) {
    return to!string(cstring.ptr);
}