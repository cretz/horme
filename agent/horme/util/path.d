module horme.util.path;

import std.path : dirName, baseName, dirSeparator;
import std.conv : to;
import std.string : toStringz;

//ref: http://forum.dlang.org/thread/bohuvfiuavsvcooocgym@forum.dlang.org#post-jro4g7:24chi:241:40digitalmars.com

version(Win32) {
    import std.c.windows.windows;
} else version(OSX) {
    private extern(C) int _NSGetExecutablePath(char* buf, uint* bufsize);
} else {
    import std.c.linux.linux;
}

/// Gets the full path to the currently running executable,
/// regardless of working directory or PATH env var or anything else.
/// Note that this is far more accurate and reliable than using args[0].
string getExec()
{
    auto file = new char[4*1024];
    size_t filenameLength;
    version (Win32) {
        filenameLength = GetModuleFileNameA(null, file.ptr, file.length - 1);
    } else version(OSX) {
        filenameLength = file.length - 1;
        _NSGetExecutablePath(file.ptr, &filenameLength);
    } else {
        //TODO: fix for BSD, see ref link above
        filenameLength = readlink(toStringz("/proc/self/exe"), file.ptr, file.length - 1);
    }
    return to!string(file[0..filenameLength]);
}

/// Like getExec, but doesn't include the path.
string getExecName() {
    return getExec().baseName();
}

/// Like getExec, but only returns the path (including trailing path separator).
string getExecPath() {
    return getExec().dirName() ~ dirSeparator;
}