module horme.util.log;

import std.stdio : writefln;

//TODO: make this more robust please

auto errorEnabled = true;
auto warnEnabled = true;
auto infoEnabled = true;
version (unittest) {
    auto traceEnabled = true;
} else {
    auto traceEnabled = false;
}

void error(S...)(lazy S args) {
    if (errorEnabled) {
        writefln(args);
    }
}

void warn(S...)(lazy S args) {
    if (warnEnabled) {
        writefln(args);
    }
}

void info(S...)(lazy S args) {
    if (infoEnabled) {
        writefln(args);
    }
}

void trace(S...)(lazy S args) {
    if (traceEnabled) {
        writefln(args);
    }
}