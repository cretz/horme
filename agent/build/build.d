
import std.array;
import std.stdio;
import std.file;
import std.algorithm;
import std.process;
import std.string;
import std.conv;

int main(string[] argv) {
    //array holding files to compile
    auto files = [
        "../main.d"
    ];
    auto ignore = [
        "index.d",
        "libevent.d",
        "ssl.d",
        "ssl_bio.d",
        "nonblocking.d",
        "AgentService_server.skeleton.d",
        "sigarlib.d"
    ];
    //add other directories
    files ~= allDFiles("../horme", ignore);
    files ~= allDFiles("../thrift", ignore);
    version (Win32) {
        files ~= "../horme/pkg/system/sigar.lib";
    } else {
        files ~= "../horme/pkg/system/libsigar.a";
    }
    //build command (assumes on PATH)
    auto cmd = "dmd " ~ text(joiner(files, " "));
    if (argv[argv.length - 1] == "unittest") {
        cmd ~= " -unittest -ofhormed-test";
    } else {
        cmd ~= " -release -O -ofhormed";
    }
    version (Win32) {
        cmd ~= ".exe";
    }
    writeln(cmd);
    writeln(shell(cmd));
    return 0;
}

string[] allDFiles(string dir, string[] ignore) {
    string[] files;
    files.reserve(50);
    foreach (string name; dirEntries(dir, SpanMode.depth)) {
        if (name.endsWith(".d")) {
            auto ok = true;
            foreach (ignorePiece; ignore) {
                if (name.endsWith(ignorePiece)) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                files ~= name.replace(`\`, "/");
            }
        }
    }
    return files;
}