# Building

## Build D thrift interface
Note: this does not have to be done unless it is changed. It is generated code and committed.

* Install the latest thrift
* Uncomment the d namepsace in the file (this is there because D isn't included in the latest stable release, so the Java one does not recognize it)
* From this directory, run "thrift -out . --gen d agentservice.thrift"

## Build sigar

* Build the lib as mentioned in horme/pkg/system/README
* Make sure the lib is in the horme/pkg/system folder before running build
* Don't forget to distribute the shared lib (if on Windows) w/ the executable

## Build the agent

* Make sure dmd2 is installed
* Inside the build/ dir, execute "dmd -run build.d"

Note: this will place the executable in the build/ dir.
Note: passing unittest as a param to the build script generates a unittest executable

# Agent

## CLI

horme [-b host:port]

* -b host:port - This simply runs the bootstrapper that downloads the config info from the controller and exits.

## Config

Every agent has a configuration, but it is usually bootstrapped from a controller. The format is in JSON. It can be overridden so do not edit locally. Here is a sample of a configuration:

{
    "id": "1234",
    "heartbeat": "5000",
    "primaryController": "111",
    "controllers": [
        {"id": "111", "host": "1.2.3.4", "port": "1234"},
        {"id": "222", host": "1.2.3.4", "port": "1234"}
    ],
    "metrics": [
        {
            "id": "1234",
            "name": "my.metric.name",
            "executionFrequency": "60000",
            "haltOnFailure": true,
            "params": {
                "key1": "val1",
                "key2": "val2"
            }
        },
        {
            "id": "4567",
            "name": "my.metric.name2",
            "executionFrequency": "120000",
            "haltOnFailure": false
        }
    ]
}

## Definining Metric Packages

A metric package is a set of metrics.

### Agent Implementation

Your implementation must implement horme.pkg.metricpackage.MetricPackage. Once you have provided the implementation, you must add your package to the factory in horme/pkg/metricpackage.d.