module main;

import std.getopt : getopt, config;
import std.array : split;
import std.conv : to;
import thrift.base;
import thrift.transport.buffered;
import thrift.codegen.client;
import thrift.transport.socket;
import thrift.protocol.binary;
import horme.util.path;
import horme.util.log;
import horme.util.config;
import horme.exception;
import horme.daemon;
import horme.thrift.AgentService;
import horme.thrift.agentservice_types;

void main(string[] argv)
{
    //die right away
    version (unittest) {
        return;
    }

    info("Starting agent");
    //get config
    string bootstrapController;
    string configFilename = getExecPath() ~ "hormed.conf";
    int threadPoolSize = 4;
    getopt(argv,
        config.noPassThrough,
        "bootstrap|b", &bootstrapController,
        "config|c", &configFilename,
        "threads|t", &threadPoolSize
    );
    //bootstrap?
    if (bootstrapController !is null) {
        loadConfigFromController(bootstrapController, configFilename);
    } else {
        info("Starting daemon w/ thread pool size %s from config file %s",
            threadPoolSize, configFilename);
        //start up daemon
        auto daemon = new Daemon(configFilename, threadPoolSize);
        daemon.start();
    }
    info("Agent ended");
}

void loadConfigFromController(string bootstrapController, string configFilename) {
    info("Downloading and saving config to %s", configFilename);
    //parse bootstrapController
    //TODO: IPv6 support
    auto pieces = bootstrapController.split(":");
    if (pieces.length == 1) {
        pieces ~= "8710";
    } else if (pieces.length != 2) {
        throw new HormeException("Invalid bootstrap format, must be HOST:PORT");
    }
    auto host = pieces[0];
    auto port = to!int(pieces[1]);
    //open service
    auto socket = new TSocket(host, cast(ushort) port);
    auto transport = new TBufferedTransport(socket);
    auto protocol = tBinaryProtocol(transport);
    auto client = tClient!AgentService(protocol);
    transport.open();
    scope (exit) transport.close();
    //grab config (blank agent ID means it will be discovered)
    auto config = client.getConfig("");
    //now save the config
    config.saveConfigToFile(configFilename);
    info("Config saved for discovered agent ID %s", config.id);
}
