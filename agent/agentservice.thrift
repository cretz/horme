
//namespace d horme.thrift
namespace java horme.thrift.agent

struct ControllerConfig {
    1: required string id,
    2: required string host,
    3: required i32 port
}

struct MetricConfig {
    1: required string id,
    2: required string name,
    3: required i64 executionFrequency,
    4: required bool haltOnFailure,
    5: optional map<string, string> params
}

struct AgentConfig {
    1: required string id,
    2: required i64 heartbeat,
    3: required string primaryControllerId,
    4: required map<string, ControllerConfig> controllers,
    5: required map<string, MetricConfig> metrics
}

struct AgentUpdate {
    1: optional AgentConfig config
}

struct MetricData {
    1: required string id,
    2: required i64 time,

    3: optional bool dataBool,
    4: optional byte dataByte,
    5: optional i16 dataI16,
    6: optional i32 dataI32,
    7: optional i64 dataI64,
    8: optional double dataDouble,
    9: optional string dataString,
    10: optional list<string> dataList,
    11: optional map<string, string> dataMap,

    12: optional string message
}

service AgentService {
    AgentConfig getConfig(1: string agentId),
    AgentUpdate ping(1: string agentId),
    AgentUpdate postMetrics(1: string agentId, 2: list<MetricData> metrics)
}