hormed(8)
=========

NAME
----
hormed - The Horme monitoring agent

SYNOPSIS
--------
`hormed` [`-b` *host*] [`-c` *file*] [`t` *poolsize*]

DESCRIPTION
-----------
The hormed agent daemon. This will run and attempt to connect to a running controller based on the configuration file. If this is a first time execution, the -b option can be used to bootstrap the agent and automatically download its configuration.

OPTIONS
-------

* `-b` *host*, `--bootstrap` *host*

  Bootstrap this agent to a controller, download the configuration, and exit. The host value is in the form of IP:PORT. If the port needs to be specified for an IPv6 address, the IP should be enclosed in square brackets. If port is not provided, 8710 is assumed.

* `-c` *file*, `--config` *file*

  The path to the configuration file. This is used for storing and/or retrieving the configuration. If this is not present, the hormed.conf file in the path of the executable is used.

* `t` *poolsize*, `--threads` *poolsize*

  The number of threads for system monitoring checks. This is best as the number of CPUs * 2. Unless this system is known to be monitoring only one type of metric, the number should be greater than 1. If this is not present, it is defaulted to 4.

CONFIGURATION
-------------

TODO: explanation of configuration and how it can be overridden.

EXAMPLES
--------

TODO

COPYRIGHT
---------

Horme is Copyright (c) 2012 Chad Retz and Kenji Fukasawa