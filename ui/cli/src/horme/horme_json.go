package horme

//please keep in alphabetical order

type AgentMetricResult struct {
	id            string
	name          string
	href          string
	agent         AgentReference
	metric        MetricReference
	haltOnFailure bool
	params        map[string]string
}

type AgentMetricUpdate struct {
	name          string
	metricId      string
	haltOnFailure bool
	params        map[string]string
}

type AgentReference struct {
	id   string
	name string
	href string
}

type AgentResult struct {
	id                string
	name              string
	href              string
	ipAddress         string
	heartbeat         int
	primaryController ControllerReference
	controllerGroup   ControllerGroupReference
	agentMetrics      []AgentResultAgentMetricResult
}

type AgentResultAgentMetricResult struct {
	id string
	name string
	href string
	metric MetricReference
	haltOnFailure bool
	params map[string]string
}

type AgentUpdate struct {
	name                string
	ipAddress           string
	heartbeat           int
	primaryControllerId string
	controllerGroupId   string
}

type ControllerGroupReference struct {
	id   string
	name string
	href string
}

type ControllerGroupResult struct {
	id          string
	name        string
	href        string
	controllers []ControllerResult
	agents      []AgentReference
}

type ControllerGroupResultControllerResult struct {
	id string
	name string
	href string
	host string
	port int
	mac string
}

type ControllerGroupUpdate struct {
	name              string
	host              string
	port              int
	mac               string
	controllerGroupId string
}

type ControllerReference struct {
	id   string
	name string
	href string
}

type ControllerResult struct {
	id              string
	name            string
	href            string
	host            string
	port            int
	mac             string
	controllerGroup ControllerGroupReference
}

type ControllerUpdate struct {
	name              string
	host              string
	port              string
	mac               string
	controllerGroupId string
}

type MetricReference struct {
	id   string
	name string
	href string
}
