package horme

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"net/http"
	"bytes"
)

type Conf struct {
	configFile  *string
	host        *string
	username    *string
	password    *string
	command     *string
	commandArgs []string
}

type ConfFile struct {
	host     *string
	username *string
	password *string
}

func main() {
	//cli args
	conf := new(Conf)
	if err := conf.parse(os.Args[1:]); err != nil {
		log.Fatal(err)
	}
	//apply conf file
	if err := conf.applyConfFile(); err != nil {
		log.Fatal(err)
	}
	//command
	var err error
	switch conf.command {
	case "agent":
		err = RunAgent(conf)
	default:
		log.Fatalf("Unknown command: %v", conf.command)
	}
}

func (conf *Conf) parse(arguments []string) error {
	var nextVal *string
	commandIndex := -1
	for index, value := range arguments {
		if nextVal != nil {
			*nextVal = value
			nextVal = nil
		} else {
			switch value {
			case "-c", "--config":
				nextVal = &conf.configFile
			case "-h", "--host":
				nextVal = &conf.host
			case "-u", "--username":
				nextVal = &conf.username
			case "-p", "--password":
				nextVal = &conf.password
			default:
				commandIndex = index
			}
			if commandIndex != -1 {
				break
			}
		}
	}
	if commandIndex == -1 {
		return errors.New("No command found")
	} else if nextVal != nil {
		return errors.New("Unexpected end of command")
	}
	conf.command = arguments[commandIndex]
	conf.commandArgs = arguments[commandIndex+1:]
	return nil
}

func (conf *Conf) applyConfFile() error {
	var file *os.File
	if conf.configFile != nil {
		file, err := os.Open(conf.configFile)
		if err != nil {
			return err
		}
	} else {
		file, _ = os.Open("horme.conf")
	}
	if file != nil {
		//load as JSON
		confFile := new(ConfFile)
		decoder := json.NewDecoder(file)
		if err := decoder.Decode(&confFile); err != nil {
			file.Close()
			return err
		}
		file.Close()
		//set values that aren't in conf
		if conf.host == nil && confFile.host != nil {
			conf.host = confFile.host
		}
		if conf.username == nil && confFile.username != nil {
			conf.username = confFile.username
		}
		if conf.password == nil && confFile.password != nil {
			conf.password = confFile.password
		}
	}
}

func (conf *Conf) buildRequest(method string, id *string, body interface{}) (*http.Request, error) {
	url := "http://" + conf.host + "/agent"
	if id != nil {
		url += "/" + id
	}
	var reader *bytes.Reader = nil
	if reader != nil {
		if byteArray, err := json.Marshal(body); err != nil {
			return nil, err
		}
		reader = bytes.NewReader(byteArray)
	}
	if req, err := http.NewRequest(method, url, reader); err != nil {
		return nil, err
	}
	if conf.username != nil && conf.password != nil {
		req.SetBasicAuth(conf.username, conf.password)
	}
	return req, nil
}