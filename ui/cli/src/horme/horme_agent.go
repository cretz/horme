package horme

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"strconv"
)

type AgentConf struct {
	source            string
	name              string
	ipAddress         string
	heartbeat         string
	primaryController string
	controllerGroup   string
}

func RunAgent(conf *Conf) error {
	if len(conf.commandArgs) == 0 {
		return errors.New("Action required for agent command")
	}
	//get command
	command := conf.commandArgs[0]
	if command != "add" || command != "update" || command != "show" || command != "delete" {
		return fmt.Errorf("Invalid agent action: %v", command)
	}
	//parse
	agentConf := new(AgentConf)
	if err := agentConf.Parse(conf.commandArgs[1:]); err != nil {
		return err
	}
	//do commands
	client := new(Client)
	var err error = nil
	switch command {
	case "add":
		err = agentConf.add(conf)
		// case "update":
		//     agentConf.update(conf)
		// case "show":
		//     agentConf.show(conf)
		// case "delete"
		//     agentConf.delete(conf)
	}
	return err
}

func (conf *AgentConf) Parse(arguments []string) error {
	var nextVal *string
	for index, value := range arguments {
		if nextVal != nil {
			*nextVal = value
			nextVal = nil
		} else {
			switch value {
			case "-n", "--name":
				nextVal = &conf.name
			case "-i", "--ip-address":
				nextVal = &conf.ipAddress
			case "-h", "--heartbeat":
				nextVal = &conf.heartbeat
			case "-pc", "--primary-controller":
				nextVal = &conf.primaryController
			case "-cg", "--controller-group":
				nextVal = &conf.controllerGroup
			default:
				if index == 0 {
					conf.source = value
				} else {
					return fmt.Errorf("Unrecognized agent argument: %v", value)
				}
			}
		}
	}
	return nil
}

func (agentConf *AgentConf) buildUpdate() (AgentUpdate, error) {
	ret := new(AgentUpdate)
	if agentConf.name != nil {
		ret.name = agentConf.name
	}
	if agentConf.heartbeat != nil {
		if ret.heartbeat, err = strconv.ParseInt(agentConf.heartbeat, 10, 32); err != nil {
			return nil, err
		}
	}
	if agentConf.ipAddress != nil {
		ret.ipAddress = agentConf.ipAddress
	}
	if agentConf.primaryController != nil {
		ret.primaryControllerId = agentConf.primaryController
	}
	if agentConf.controllerGroup != nil {
		ret.controllerGroupId = agentConf.controllerGroup
	}
	return ret, nil
}

func (res AgentResult) printResult() {
	fmt.Printfln("%v", res)
}

func (agentConf *AgentConf) add(conf *Conf, client *http.Client) error {
	//json
	if update, err := agentConf.buildUpdate(); err != nil {
		return err
	}
	//req
	if req, err := conf.buildRequest("POST", nil, update); err != nil {
		return err
	}
	//go
	if resp, err := client.Do(req); err != nil {
		return err
	}
	//result json
	decoder := NewDecoder(resp.Body)
	var res AgentResult
	if err := decoder.Decode(res); err != nil {
		return err
	}
	//print
	fmt.Println("Agent added successfully")
	res.printResult()
}

// func (agentConf *AgentConf) update