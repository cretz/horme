package horme

import (
	"testing"
)

func TestParse(t *testing.T) {
	conf := new(Conf)
	//simple test for all short values
	err := conf.Parse([]string{
		"-c", "blah.conf",
		"-h", "localhost:1234",
		"-u", "user",
		"-p", "pass",
		"agent", "-a", "agentparam",
	})
	var validate = func(t *testing.T, conf *Conf) {
		if conf.configFile != "blah.conf" ||
			conf.host != "localhost:1234" ||
			conf.username != "user" ||
			conf.password != "pass" ||
			conf.command != "agent" ||
			len(conf.commandArgs) != 2 ||
			conf.commandArgs[0] != "-a" ||
			conf.commandArgs[1] != "agentparam" {

			t.Fatalf("Bad conf: %v", conf)
		}
	}
	if err != nil {
		t.Fatal(err)
	}
	validate(t, conf)
	//long values
	err = conf.Parse([]string{
		"--config", "blah.conf",
		"--host", "localhost:1234",
		"--username", "user",
		"--password", "pass",
		"agent", "-a", "agentparam",
	})
	if err != nil {
		t.Fatal(err)
	}
	validate(t, conf)
}
