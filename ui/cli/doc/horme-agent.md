horme-agent(1)
==============

NAME
----
horme-agent - Add, edit, view, or delete agents

SYNOPSIS
--------
`horme` [*options*] `agent add` [`-n` *name*] [`-i` *ip*] [`-h` *ms*] [`-pc` *id*|*name*] [`-cg` *id*|*name*]

`horme` [*options*] `agent update` *id*|*name* [`-n` *name*] [`-i` *ip*] [`-h` *ms*] [`-pc` *id*|*name*] [`-cg` *id*|*name*]

`horme` [*options*] `agent show` [*id*|*name*]

`horme` [*options*] `agent delete` *id*|*name*

DESCRIPTION
-----------
This command adds, updates, views, or deletes agents

OPTIONS
-------

* *id*|*name*

  The ID or name of the agent to update, show, or delete. If this is not set when showing, the entire agent list is shown.

* `-n` *name*, `--name` *name*

  The desired name of the agent for reference purposes. The maximum length is 255 characters. It must be unique within the controller group.

* `-i` *ip*, `--ip-address` *ip*

  The IP address of the agent. This can be an IPv4 address or an IPv6 address (in expanded or collapsed form). This is only required to enable easy bootstrapping from the agent. Otherwise, it is populated the first time the agent checks in.

* `-h` *ms*, `--heartbeat` *ms*

  The frequency in milliseconds this agent communicates with the controller. If it is not specified when adding, it is defaulted to 30000 (30 seconds).

* `-pc` *id*|*name*, `--primary-controller` *id*|*name*

  The ID or name of the primary controller to use for this agent. If a primary controller is not specified when adding, it is uses the group of this controller to determine the most underused controller.

* `-cg` *id*|*name*, `--controller-group` *id*|*name*

  The ID or name of the controller group to assign to this agent. This is not allowed if a primary controller is provided. If neither this nor the primary controller is provided (when adding), it is assumed to belong to this controller's group.

COPYRIGHT
---------

Horme is Copyright (c) 2012 Chad Retz and Kenji Fukasawa

SEE ALSO
--------
horme(1)