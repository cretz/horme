horme(1)
========

NAME
----
horme - Control the Horme monitoring system

SYNOPSIS
--------
`horme` [`-c` *file*] [`-h` *host*] [`-u` *username*] [`-p` *password*] *commands*...

DESCRIPTION
-----------
The main horme executable. This simply documents the options you can pass before executing any commands. The `horme` command cannot be called on its own.

OPTIONS
-------

* `-c` *file*, `--config` *file*

  The path to a config file. Configuration files are not required. However, if this is not set, one will be looked for in the current working directory named "horme.conf".

* `-h` *host*, `--host` *host*

  The host of the controller to connect to. This is in the form IP:PORT. If the port needs to be specified for an IPv6 address, the IP should be enclosed in square brackets. If port is not provided, 8700 is assumed. If no host is provided and there is not one in a configuration file, localhost is assumed.

* `-u` *username*, `--username` *username*

  The username of user to connect to the controller as. This is not required for setups that do not have user-based authentication.

* `-p` *password*, `--password` *password*

  Use this password as the password to connect to the controller with. This is not required for setups that do not have user-based authentication. If the password is not present here or in the configuration, but username is present here or in the configuration, the user will be prompted for a password.

CONFIGURATION
-------------

TODO: Explain the configuration file, the format, how it works, etc.

COPYRIGHT
---------

Horme is Copyright (c) 2012 Chad Retz and Kenji Fukasawa

SEE ALSO
--------
horme-agent(1), horme-agent-metric(1)