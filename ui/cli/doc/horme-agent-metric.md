horme-agent-metric(1)
=====================

NAME
----
horme-agent-metric - Add, edit, view, or delete agent metrics

SYNOPSIS
--------
`horme` [*options*] `agent-metric add` *agent* [`-n` *name*] `-m` *metric* [`-h` *true*|*false*] [-p *name* *value*]...

`horme` [*options*] `agent-metric update` [*agent*] *id*|*name* [`-n` *name*] [`-m` *metric*] [`-h` *true*|*false*] [-p *name* *value*]...

`horme` [*options*] `agent show` [*agent*] [*id*|*name*]

`horme` [*options*] `agent delete` [*agent*] *id*|*name*

DESCRIPTION
-----------
This command adds, updates, views, or deletes agent metrics

OPTIONS
-------

* *agent*

  The ID or name of the agent the metric applies to. This is required for `add`. For `update` and `delete`, this would be used in conjunction with *name* to qualify the name when looking the agent metric metric up. For `show`, this can be used just to show all of an agent's metrics.

* *id*|*name*

  The ID or name of the agent metric to update, show, or delete. If this is not set when showing, the entire agent metric list is shown (possible constrained by *agent*). An error occurs if *name* returns multiple results (use *agent* to qualify which agent).

* `-n` *name*, `--name` *name*

  The desired name of the agent metric for reference purposes. The maximum length is 255 characters. It must be unique within the agent.

* `-m` *metric*, `--metric` *metric*

  The ID or name of the core metric to add/update. This is required when adding.

* `-h` *true*|*false*, `--halt-on-failure` *true*|*false*

  Whether or not to stop attempting to poll for this agent metric if it is unable to be obtained (until reset). When adding, the default is true.

* `-p` *name=*value*, `--param` *name* *value*

  Parameters for the agent metric. Some metrics require parameters, some have optional parameters, and some do not have parameters at all. All parameters require values. For values that are a list, they should be comma-separated. For values that are a key-value set, the entries should be comma-separated and the key should be separated from the value via an equals sign (keys and/or values can be quoted, but make sure to escape the quotes if your operating system requires it).

COPYRIGHT
---------

Horme is Copyright (c) 2012 Chad Retz and Kenji Fukasawa

SEE ALSO
--------
horme(1)